﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using mshtml;
using System.IO.Compression;
using System.Web.Script;
using System.Web.Script.Serialization;


namespace WindowsFormsApplication3
{
    
    public partial class Form1 : Form
    {

        Server s; // = new Server(21770);
        Thread ServerThread;
        public static int JoinSecondsGSPNAuth = 40;

        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            s = new Server(21770);
            ServerThread = new Thread(new ThreadStart(s.StartServer));
            ServerThread.Start();
            ServerThread.IsBackground = true;
            ServerStatus.Text = "Сервер запущен";
            textBox1.Text = JoinSecondsGSPNAuth.ToString(); // "35";
           
            //s = new Server(21770); 
            //ServerThread = new Thread(() => s); //new Thread(() => new Server(21770));
            //ServerStatus.Text = "Сервер остановлен";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (s != null) { return; }
            s = new Server(21770);
            ServerThread = new Thread(new ThreadStart(s.StartServer));
            ServerThread.Start();
            ServerThread.IsBackground = true;
            ServerStatus.Text = "Сервер запущен";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (s == null) { return; }
            s.IsRunning = false;
            string url = "http://127.0.0.1:21770";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            try
            {
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                tS.Close();
            }
            catch (Exception)
            { }
            ServerThread.Abort();
            s = null;
            ServerStatus.Text = "Сервер остановлен";
        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            JoinSecondsGSPNAuth = Convert.ToInt32(textBox1.Text); 
        }
    }

    class Client
    {
        public static CookieContainer GSPNCoockies;
        //public static CookieContainer GSPNCoockiesStandBy;
        public static DateTime GSPNCoockiesStandByNextDate;
        public static bool WaitGettingCookies;

        public static string ZPO;//параметр для проверки ключевых деталей
        public static DateTime ZPOdate;//дата получения ZPO

        private void ForThreadFreshCookies()
        {
            while (true)
            {
                if (GSPNCoockiesStandByNextDate < DateTime.Now)
                {
                    
                    //foreach (Control che in ctiveForm.Controls)
                    //{
                    //    if (che.GetType().ToString().IndexOf("CheckBox") > -1)
                    //    {
                    //        CheckBox che1 = (CheckBox)che;
                    //        if (che1.Name == "checkBoxMorningAuthorizationGSPN") //добавляешь еще проверку
                    //            {
                    //                if (che1.Checked == true)
                    //                {
                   
                                        AddToLogFile("Пришло время проверить актульность сессии. Начало рабочего дня.");
                                        if (CheckGSPNAuthorization())
                                        {
                                            RefreshNextDayForStandByCookie();
                                        }
                                        else
                                        {
                                            GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddMinutes(10);
                                            //GSPNCoockiesStandByNextDate = DateTime.Now;
                                            //Random rnd = new Random();
                                            //int RandMinutes = rnd.Next(60, 90);
                                            //GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddMinutes(RandMinutes);
                                            AddToLogFile("Неуспешная попытка получить куки. Дата получения кук передвинута на 10 минут " + GSPNCoockiesStandByNextDate.ToString());
                                        }

                    //                }
                    //                else
                    //                {
                    //                    AddToLogFile("Не авторизуемся с утра на ГСПН т.к. не установлен флажок. Перенесем дату на сутки.");
                    //                    RefreshNextDayForStandByCookie();
                    //                }
                    //            }
                    //    }
                    //}

                    
                }
                else
                {
                    Application.DoEvents();
                }
            }
        }

        public void GetGSPNCoockies()
        {
            WaitGettingCookies = true;
            if (GSPNCoockiesStandByNextDate == DateTime.MinValue)
            {
                GSPNCoockiesStandByNextDate = DateTime.Today;
                GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddHours(8);
                Random rnd = new Random();
                int RandMinutes = rnd.Next(50, 59);
                GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddMinutes(RandMinutes);
               
                if (DateTime.Now.Hour > 8)
                {
                    RefreshNextDayForStandByCookie();
                }
                //Thread FreshCookies = new Thread(() => ForThreadFreshCookies());
                //FreshCookies.IsBackground = true;
                //FreshCookies.Start();
            }
            
            Thread ThA = new Thread(
                                       () =>
                                       {
                                           Form2 AuthorizationForm = new Form2();
                                           AuthorizationForm.Show();
                                           GSPNCoockies = AuthorizationForm.Authorization();
                                           Application.Run();
                                       }
                                   );
            //ThA.SetApartmentState(ApartmentState.STA);
            //ThA.Start();
            //while (Form2.finish != true)
            //{
            //    Application.DoEvents();
            //}
            //ThA.Abort();
            //Form2.finish = false;
            //GSPNCoockiesTR = GSPNCoockies;

            ThA.SetApartmentState(ApartmentState.STA);
            ThA.Start();

            ThA.Join(TimeSpan.FromSeconds(Form1.JoinSecondsGSPNAuth)); // 35));
            AddToLogFile("Подождали 35 секунд");
            Form2.sendEnter();
            AddToLogFile("Отправили Enter в браузер");
            //if (Form2.finish == true)
            //{
            //    ThA.Abort();
            //}
            
            ThA.Abort();
            AddToLogFile("Разорвали поток с бразуером");
            
            Form2.finish = false;
            ZPOdate = new DateTime();
            
            //string responseFromServer = ADVPartsInfoRequest("GH97-14630A");
            //if (responseFromServer.IndexOf("Your session has expired") != -1)
            //{
            //    WaitGettingCookies = false;
            //    return; 
            //}
            //else
            //{ 
            //    MakeStandByCookies(); 
            //}

            WaitGettingCookies = false;
        }

        //public void UpdateStandByCookies()
        //{
        //    string responseFromServer = ADVPartsInfoRequest("GH97-14630A", false);//проверим живы ли основные куки.
        //    while (responseFromServer.IndexOf("Your session has expired") != -1)
        //        {
        //            if (WaitGettingCookies)//значит уже получаются куки надо подождать
        //            {
        //                while (WaitGettingCookies)
        //                {
        //                    Application.DoEvents();
        //                }
        //                responseFromServer = ADVPartsInfoRequest("GH97-14630A", false);
        //            }
        //            else
        //            {
        //                GetGSPNCoockies();
        //                responseFromServer = ADVPartsInfoRequest("GH97-14630A", false);
        //            }
        //        }
        //    AddToLogFile("Основные куки живы. обновляем резервыне куки");
        //    RefreshNextDayForStandByCookie();
        //    Thread forCookie = new Thread(() => MakeStandByCookies());
        //    forCookie.IsBackground = true;
        //    forCookie.Start();
        //}

        public void GSPNLogOut()
        {

            string url = "https://gspn5.samsungcsportal.com/basis/loginOut.do?method=service";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            //tQ.Method = "GET";
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Referer = "https://gspn5.samsungcsportal.com/basis/common/topMenu.jsp";
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            tQ.Headers.Add("Upgrade-Insecure-Requests", "1");
            try
            {
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                tS.Close();
                AddToLogFile("LogOut1 - без Catch");
            }
            catch
            {
                AddToLogFile("LogOut1 - !Catch!");
            }

            url = "https://gspn5.samsungcsportal.com/basis/login/loginOut.jsp ";
            tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            //tQ.Method = "GET";
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Referer = "https://gspn5.samsungcsportal.com/basis/common/topMenu.jsp";
            ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            tQ.Headers.Add("Upgrade-Insecure-Requests", "1");
            try
            {
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                tS.Close();
                AddToLogFile("LogOut2 - без Catch");
            }
            catch
            {
                AddToLogFile("LogOut2 - !Catch!");
            }

            GSPNCoockies = new CookieContainer();
            AddToLogFile("GSPNCoockies = = new CookieContainer() - конец LogOut");
        }
        
        
        public bool CheckGSPNAuthorization()
        {
            string responseFromServer = "";
            int FailCount = 0;
            while (GSPNCoockies == null)
            {
                AddToLogFile("CheckGSPNAuthorization кук не существует");
                FailCount = FailCount + 1;
                if (FailCount > 3)
                {
                    return false;
                }
                if (WaitGettingCookies)//значит уже получаются куки надо подождать
                {
                    AddToLogFile("CheckGSPNAuthorization куки получаются. ждем");
                    while (WaitGettingCookies)
                    {
                        Application.DoEvents();
                    }
                }
                else
                {
                    GetGSPNCoockies(); 
                    //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 255"); //— полная очистка кэша браузера
                    //System.Diagnostics.Process.Start("rundll32.exe",  "InetCpl.cpl,ClearMyTracksByProcess 8"); //— удаление временных файлов
                    //System.Diagnostics.Process.Start("rundll32.exe",  "InetCpl.cpl,ClearMyTracksByProcess 1"); //— удаление журнала посещений
                    //System.Diagnostics.Process.Start("rundll32.exe",  "InetCpl.cpl,ClearMyTracksByProcess 16"); //— удаление данных веб-форм
                    //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 32"); //— удаление сохраненных паролей
                }
            }

            AddToLogFile("CheckGSPNAuthorization_(GSPNCoockies не null) НАЧАЛО_Протестируем основные куки. Остановимся на 3 секунды.");
            //System.Threading.Thread.Sleep(5000);
            responseFromServer = ADVPartsInfoRequest("GH97-14630A",false);
            AddToLogFile("CheckGSPNAuthorization_Есть результат первого запроса - " + responseFromServer);
            //if (responseFromServer.IndexOf("Your session has expired") != -1)
            //{
            //   if (GSPNCoockiesStandBy != null)
            //    {
            //        AddToLogFile("Обращение к CheckGSPNAuthorization. Есть запасные куки. Тестируем их");
            //        responseFromServer = ADVPartsInfoRequest("GH97-14630A", true);//подменим куки из резервной сессии и попытаемся снова
            //        if (responseFromServer.IndexOf("Your session has expired") == -1) //сработали запасные куки
            //        {
            //            AddToLogFile("подменили куки из резервной сессии и сервер дал нужный ответ. запускаем процесс создания резервных куки. Переводим робота на резервную сессию");
            //            GSPNCoockies = GSPNCoockiesStandBy; //устанавливаем рабочими запасные куки
            //            // MakeStandByCookies();
            //            GSPNCoockiesStandBy = null;//обнулим куки
            //            //return;  
            //        }

            //    }
            //}
            FailCount = 0;//обнулим счетчик ошибок, т.к. куки получены.
            if (responseFromServer.IndexOf("Your session has expired") != -1)
            {
                GSPNLogOut();
            }
            while (responseFromServer.IndexOf("Your session has expired") != -1)
            {
                AddToLogFile("CheckGSPNAuthorization получен плохой отет по запросу ADVPartsInfoRequest(GH97-14630A,false); резервные куки не сработали. Запускаем механизм получения кук");
                FailCount = FailCount + 1;
                if (FailCount > 3)
                {
                    return false;
                }
                if (WaitGettingCookies)//значит уже получаются куки надо подождать
                {
                    while (WaitGettingCookies)
                    {
                        Application.DoEvents();
                    }
                    responseFromServer = ADVPartsInfoRequest("GH97-14630A",false);
                }
                else
                {
                    GetGSPNCoockies();
                    System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 255"); //— полная очистка кэша браузера
                    //System.Diagnostics.Process.Start("rundll32.exe",  "InetCpl.cpl,ClearMyTracksByProcess 8"); //— удаление временных файлов
                    //System.Diagnostics.Process.Start("rundll32.exe",  "InetCpl.cpl,ClearMyTracksByProcess 1"); //— удаление журнала посещений
                    //System.Diagnostics.Process.Start("rundll32.exe",  "InetCpl.cpl,ClearMyTracksByProcess 16"); //— удаление данных веб-форм
                    //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 32"); //— удаление сохраненных паролей
                    responseFromServer = ADVPartsInfoRequest("GH97-14630A",false);
                }
            }
            AddToLogFile("CheckGSPNAuthorization завершен");
            return true;
        }

        //public void MakeStandByCookies()
        //{
        //    WaitGettingCookies = true;
        //    string responseFromServer = "ForAnswer_Your session has expired";
        //    int count = 0;
        //    while (responseFromServer.IndexOf("Your session has expired") != -1)
        //    {
        //        count = count + 1;
        //        if (count > 3) 
        //        {
        //            WaitGettingCookies = false;
        //            AddToLogFile("Резервные куки не получены после трех попыток");
        //            return; 
        //        }
        //        AddToLogFile("получаем резервные куки. Попытка" + count.ToString());
        //        Thread ThA = new Thread(
        //                                   () =>
        //                                   {
        //                                       Form2 AuthorizationForm = new Form2();
        //                                       AuthorizationForm.Show();
        //                                       GSPNCoockiesStandBy = AuthorizationForm.Authorization();
        //                                       Application.Run();
        //                                   }
        //                               );


        //        ThA.SetApartmentState(ApartmentState.STA);
        //        ThA.Start();
        //        ThA.Join(65000);
        //        Form2.sendEnter();
        //        ThA.Abort();
        //        Form2.finish = false;
        //        if (GSPNCoockiesStandBy != null)
        //        {
        //            responseFromServer = ADVPartsInfoRequest("GH97-14630A", true);
        //        }
        //    }
        //    AddToLogFile("получены резервные куки на попытке" + count.ToString());
        //    RefreshNextDayForStandByCookie();
        //    WaitGettingCookies = false;
        //}

        private void AddToLogFile(string LOGvalue)
        {
            string writePath = "www/log.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(DateTime.Now + " " + LOGvalue);
                    //sw.Write(4.5);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }


        private void RefreshNextDayForStandByCookie()
        {

            GSPNCoockiesStandByNextDate = DateTime.Today;
            GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddHours(32);
            Random rnd = new Random();
            int RandMinutes = rnd.Next(50, 59);
            GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddMinutes(RandMinutes);
            
            
            
            //GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddDays(1); 
            //GSPNCoockiesStandByNextDate = DateTime.Now;
            //Random rnd = new Random();
            //int RandMinutes = rnd.Next(60, 90);
            //GSPNCoockiesStandByNextDate = GSPNCoockiesStandByNextDate.AddMinutes(RandMinutes);
            AddToLogFile("Обновлена дата следующей проверки кук на начало рабочего дня " + GSPNCoockiesStandByNextDate.ToString());
        }

        private CookieContainer MakeCookie(CookieCollection inCookCol, bool NeeddtPC, string dtPCValue)
        {
            CookieContainer cookieContForRequest = new CookieContainer();
            foreach (Cookie cookie in inCookCol) //Split(';')
            {

                cookieContForRequest.Add(cookie);
                //string name = cookie.Name;
                //if (name == "EUOBGSPNSESSIONID") 
                //{ EUOBGSPNSESSIONID = cookie; }
                //else if (name == "WMONID")
                //{ EUOBGSPNSESSIONID = cookie; }
                ////string value = cookie.Value;
                ////string path = cookie.Path;
                ////string domain = cookie.Domain; //change to your domain name
                ////GSPNCoockies.Add(new Cookie(name.Trim(), value.Trim(), path, domain));
            }
            if (NeeddtPC)
            {
                cookieContForRequest.Add(new Cookie("dtPC", "368319075_154" + dtPCValue, "/", "biz5.samsungcsportal.com"));
            }
            else
            {
                cookieContForRequest.Add(new Cookie("dtPC", "-", "/", "biz5.samsungcsportal.com"));
            }

            return cookieContForRequest;
        
        }

        public void GetZPO()
        {
            //получим ZPO для ключевых деталей  
            string responseFromServer = "";
            //while (responseFromServer.IndexOf("Your session has expired") != -1)
            //{
                //GetGSPNCoockies();
                //responseFromServer = "";
                string url = "https://biz5.samsungcsportal.com/master/part/KeyPartInfo.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
                HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
                //tQ.Method = "GET";
                tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
                tQ.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
                tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                tQ.Referer = "https://biz5.samsungcsportal.com/gspn/operate.do";
                CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
                ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
                tQ.CookieContainer = new CookieContainer();
                tQ.CookieContainer = MakeCookie(ckCol, false, "");
                tQ.Headers.Add("DNT", "1");
                tQ.KeepAlive = true;
                tQ.Headers.Add("Upgrade-Insecure-Requests", "1");
                //System.IO.Stream sendStream = tQ.GetRequestStream();
                //sendStream.Write(bytes, 0, bytes.Length);
                //sendStream.Close();
                try
                {
                    HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                    System.IO.Stream ReceiveStream = tS.GetResponseStream();
                    //System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                    //responseFromServer = sr.ReadToEnd();
                    using (GZipStream decompressionStream = new GZipStream(ReceiveStream, CompressionMode.Decompress))
                    using (var reader = new StreamReader(decompressionStream, Encoding.GetEncoding("UTF-8")))
                        responseFromServer = reader.ReadToEnd();
                    tS.Close();
                    ZPO = responseFromServer.Substring(responseFromServer.IndexOf("name='zpo' value='") + 18, 64);
                    ZPOdate = DateTime.Now;
                    AddToLogFile("Обновлена дата ZPOdate");
                }
                catch 
                {
                    AddToLogFile("СРАБОТАЛ CATCH в GetZPO()" + responseFromServer); 
                
                }
            
        }
        // Обработка входящего соединения. Конструктор класса. Ему нужно передавать принятого клиента от TcpListener
        public Client(TcpClient Client)
        {
            // Объявим строку, в которой будет хранится запрос клиента
            string Request = ""; 
            // Буфер для хранения принятых от клиента данных
            //byte[] Buffer = new byte[1024];
            byte[] Buffer = new byte[Client.ReceiveBufferSize];
            // Переменная для хранения количества байт, принятых от клиента
            int Count = 0;
            // Читаем из потока клиента до тех пор, пока от него поступают данные
            try
            {
                while ((Count = Client.GetStream().Read(Buffer, 0, Client.ReceiveBufferSize)) > 0)
                {
                    // Преобразуем эти данные в строку и добавим ее к переменной Request
                    //Request += Encoding.ASCII.GetString(Buffer, 0, Count);
                    Request += Encoding.UTF8.GetString(Buffer, 0, Count);
                    if (Request.IndexOf("</MESSAGE>") >= 0)// добавить потом условие на count. По идее он должен быть равен content-lenght
                    {
                        break;
                    }

                }
            }
            catch
            {
                AddToLogFile("обрыв связи с клиентом");
                Request = "";

            }
            
            //Console.WriteLine("Получен запрос:   " + Request);
            string OutXMLPath = "www/";
            string MessageKeyString = "";

            int StartXML = Request.IndexOf("<?xml version=");
            if (StartXML > -1)
            {
                string XMLData = Request.Substring(StartXML);
                XmlDocument InXML = new XmlDocument(); //это на библиотеке System.XML
                InXML.LoadXml(XMLData);
                var messageType = InXML.SelectSingleNode("MESSAGE/HEADER/messageType");
                MessageKeyString = InXML.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText;

                if (messageType != null && MessageKeyString.Length > -1)
                {
                    OutXMLPath = OutXMLPath + MessageKeyString + ".xml";

                    if (messageType.InnerText == "IP_EWARRANTY")
                    {
                        EwarrantyMethod(InXML, OutXMLPath);
                    }
                    else
                    {
                        if (messageType.InnerText == "CRM_SMSLIST")
                        {
                            SMSList(InXML, OutXMLPath);
                        }
                        else
                        {
                            if (messageType.InnerText == "SEND_EMAIL")
                            {
                                SendEMail(InXML, OutXMLPath);
                            }
                            else
                            {
                                if (GSPNCoockies == null) //проверяем авторизацию на сайте гспн. далее все методы про ГСПН
                                {
                                if (WaitGettingCookies)//значит уже получаются куки надо подождать и потом сразу приступать к методу
                                {
                                    AddToLogFile("GSPNCoockies == null и WaitGettingCookies истина");
                                    while (WaitGettingCookies)
                                    {
                                        Application.DoEvents();
                                    }
                                }
                                else
                                {
                                    //AddToLogFile("GSPNCoockies == null. Запрашиваем куки");
                                    //GetGSPNCoockies();
                                    //AddToLogFile("Куки получены. Проверяем авторизацию из клиента");
                                    //CheckGSPNAuthorization();
                                    AddToLogFile("GSPNCoockies == null. Запрашиваем куки");
                                    if (!CheckGSPNAuthorization())
                                    {
                                        CreateXMLFileForAnswer(OutXMLPath);
                                        XmlDocument OutXML = new XmlDocument();
                                        OutXML.Load(OutXMLPath);
                                        WriteHeadOutXML(OutXML, "fail", MessageKeyString, messageType.InnerText);
                                        string MessageTextError = "GSPN authorization fail";
                                        AddMessageToHeader(OutXML, MessageTextError);
                                        OutXML.Save(OutXMLPath);
                                        SendAnswer(Client, OutXMLPath);
                                        return;
                                    }
                                }
                            } //далее все методы связаны с ГСПН
                            
                            if (messageType.InnerText == "ADV_PARTS_INFO")
                            {
                                ADVPartsInfo(InXML, OutXMLPath);
                            }
                            else
                            {
                                if (messageType.InnerText == "ALL_WTY_INFO_BY_SERIAL_IMEI")
                                {
                                    AllWtyInfoBySerialImei(InXML, OutXMLPath);
                                }
                                else
                                {
                                    if (messageType.InnerText == "GET_MODEL_INFO")
                                    {
                                        GetModelInfo(InXML, OutXMLPath);
                                    }
                                    else
                                    {
                                        if (messageType.InnerText == "GET_WTY_INFO_BY_MODEL_SERIAL")
                                        {
                                            GetWtyInfoByModelSerial(InXML, OutXMLPath);
                                        }
                                        else
                                        {
                                            if (messageType.InnerText == "CHANGE_GSPN_CUSTOMER")
                                            {
                                                ChangeGSPNCustomer(InXML, OutXMLPath);
                                            }
                                            else
                                            {
                                                if (messageType.InnerText == "ADV_ORDER_INFO")
                                                {
                                                    ADVOrderInfo(InXML, OutXMLPath);
                                                }
                                                else
                                                {
                                                    if (messageType.InnerText == "PO_SHIPPING_INFORMATION")
                                                    {
                                                        POShippingInformation(InXML, OutXMLPath);
                                                    }
                                                    else
                                                    {
                                                        if (messageType.InnerText == "ADV_MGD_INFO")
                                                        {
                                                            ADVMGDinfo(InXML, OutXMLPath);
                                                        }
                                                        else
                                                        {
                                                            if (messageType.InnerText == "ADV_MGD_DETAIL")
                                                            {
                                                                ADVMGDDetail(InXML, OutXMLPath);
                                                            }
                                                            else
                                                            {
                                                                if (messageType.InnerText == "ADV_SPR")
                                                                {
                                                                    ServicePartReturn(InXML, OutXMLPath, XMLData);
                                                                }
                                                                else
                                                                {
                                                                        if (messageType.InnerText == "ADV_ST_DELPART")
                                                                        {
                                                                            ADVSTDELPART(InXML, OutXMLPath, XMLData);
                                                                        }
                                                                        else
                                                                        {
                                                                            if (messageType.InnerText == "ADV_IMEI_FABRIC_INFO")
                                                                            {
                                                                                ADV_IMEIFabricInfo(InXML, OutXMLPath);
                                                                            }
                                                                        }
                                                                 }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }

                                        }
                                    }

                                }

                            }
                        }
                    }
                }
                }
                else//(messageType != null)
                {
                    SendError(Client, 400);
                    AddToLogFile("ВАЖНО!messageType == null");
                    return;
                }
            }
            else //(StartXML > -1) нет хмл в запросе
            {
                SendError(Client, 400);
                AddToLogFile("ВАЖНО!Нет XML в запросе");
                return;
            }

            // После работы метода в пути OutXMLPath лежит файл ответа. Отправим его клиенту 
            SendAnswer(Client, OutXMLPath);
            
        }

        //создает основу ответного файла
        private void CreateXMLFileForAnswer(string OutXMLPath)
        {
            XmlTextWriter SA = new XmlTextWriter(OutXMLPath, Encoding.UTF8);
            SA.WriteStartDocument();
            SA.WriteStartElement("MESSAGE");
            SA.WriteEndElement();
            SA.Close();
        }
        // записывает стандатрный HEADER в ответный файл
        private void WriteHeadOutXML(XmlDocument OutXML, string Status, string StringMessageKey, string StringMessageType)
        {
            XmlNode HEAD = OutXML.CreateElement("HEADER");
            OutXML.DocumentElement.AppendChild(HEAD);
            XmlNode headerType = OutXML.CreateElement("headerType");
            HEAD.AppendChild(headerType);
            XmlAttribute statusAtr = OutXML.CreateAttribute("status");
            statusAtr.Value = Status;
            headerType.Attributes.Append(statusAtr);
            XmlNode messageType = OutXML.CreateElement("messageType");
            messageType.InnerText = StringMessageType;
            HEAD.AppendChild(messageType);
            XmlNode messageKey = OutXML.CreateElement("messageKey");
            messageKey.InnerText = StringMessageKey;
            HEAD.AppendChild(messageKey);
        }

        //добавляет мессадж пользователю при ошибке
        private void AddMessageToHeader(XmlDocument OutXML, string MessageText)
        {
            XmlNode Header = OutXML.SelectSingleNode("MESSAGE/HEADER");
            XmlNode Message = OutXML.CreateElement("message");
            Message.InnerText = MessageText;
            Header.AppendChild(Message);
        }

        // Отправка страницы с ошибкой
        public void SendError(TcpClient Client, int Code)
        {
            // Получаем строку вида "200 OK"
            // HttpStatusCode хранит в себе все статус-коды HTTP/1.1
            string CodeStr = Code.ToString() + " " + ((HttpStatusCode)Code).ToString();
            // Код простой HTML-странички
            string Html = "<html><body><h1>" + CodeStr + "</h1></body></html>";
            // Необходимые заголовки: ответ сервера, тип и длина содержимого. После двух пустых строк - само содержимое
            string Str = "HTTP/1.1 " + CodeStr + "\nContent-type: text/html\nContent-Length:" + Html.Length.ToString() + "\n\n" + Html;
            // Приведем строку к виду массива байт
            byte[] Buffer = Encoding.ASCII.GetBytes(Str);
            // Отправим его клиенту
            try
            {
                Client.GetStream().Write(Buffer, 0, Buffer.Length);
            }
            catch
            {
                AddToLogFile("ВАЖНО!пытались отправить SendError ответ клиенту. сработал catch SendError()");
                //SendError(Client, 500);
                //return;
            }

            // Закроем соединение
            Client.Close();
        }

        //отправляет ответ клиенту
        private void SendAnswer(TcpClient Client, string OutXMLPath)
        {
            int Count;
            byte[] Buffer = new byte[1024];
            // Тип содержимого
            string ContentType = "text/xml";

            //Получаем расширение файла из строки запроса
            // string Extension = OutXMLPath.Substring(OutXMLPath.LastIndexOf('.'));

            //// Пытаемся определить тип содержимого по расширению файла
            //switch (Extension)
            //{
            //    case ".htm":
            //    case ".html":
            //        ContentType = "text/html";
            //        break;
            //    case ".css":
            //        ContentType = "text/stylesheet";
            //        break;
            //    case ".js":
            //        ContentType = "text/javascript";
            //        break;
            //    case ".jpg":
            //        ContentType = "image/jpeg";
            //        break;
            //    case ".jpeg":
            //    case ".png":
            //    case ".gif":
            //        ContentType = "image/" + Extension.Substring(1);
            //        break;
            //    default:
            //        if (Extension.Length > 1)
            //        {
            //            ContentType = "application/" + Extension.Substring(1);
            //        }
            //        else
            //        {
            //            ContentType = "application/unknown";
            //        }
            //        break;
            //}

            // Открываем файл, страхуясь на случай ошибки
            FileStream FS;
            try
            {
                FS = new FileStream(OutXMLPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                // Посылаем заголовки
                string Headers = "HTTP/1.1 200 OK\nContent-Type: " + ContentType + "\nContent-Length: " + FS.Length + "\n\n";
                byte[] HeadersBuffer = Encoding.ASCII.GetBytes(Headers);
                Client.GetStream().Write(HeadersBuffer, 0, HeadersBuffer.Length);

                // Пока не достигнут конец файла
                while (FS.Position < FS.Length)
                {
                    // Читаем данные из файла
                    Count = FS.Read(Buffer, 0, Buffer.Length);
                    // И передаем их клиенту
                    Client.GetStream().Write(Buffer, 0, Count);
                }

                // Закроем файл и соединение
                FS.Close();
            }
            catch (Exception)
            {
                // Если случилась ошибка, посылаем клиенту ошибку 500
                AddToLogFile("ВАЖНО!пытались отправить ответ клиенту. сработал catch SendAnswer()");
                SendError(Client, 500);
                return;
            }

           
            Client.Close();
        }


        //МЕТОДЫ
        //отправка e-mail
        private void SendEMail(XmlDocument RecievedRequest, string OutXMLPath)
        {

            /// <summary>
            /// Отправка письма на почтовый ящик C# mail send
            /// </summary>
            /// <param name="smtpServer">Имя SMTP-сервера</param>
            /// <param name="from">Адрес отправителя</param>
            /// <param name="password">пароль к почтовому ящику отправителя</param>
            /// <param name="mailto">Адрес получателя</param>
            /// <param name="caption">Тема письма</param>
            /// <param name="message">Сообщение</param>
            /// <param name="attachFile">Присоединенный файл</param>
            ///

            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            string mailto = RecievedRequest.SelectSingleNode("MESSAGE/BODY/EMAIL_REQUEST/mailTo").InnerText;
            string subject = RecievedRequest.SelectSingleNode("MESSAGE/BODY/EMAIL_REQUEST/subject").InnerText;
            string message = RecievedRequest.SelectSingleNode("MESSAGE/BODY/EMAIL_REQUEST/message").InnerText;
            string IsBodyHtml = RecievedRequest.SelectSingleNode("MESSAGE/BODY/EMAIL_REQUEST/IsBodyHtml").InnerText;

            string from = "SamsungInformation@poly-sc.ru";
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                foreach (string email in mailto.Split(';'))
                {
                    mail.To.Add(new MailAddress(email));
                }
                mail.Subject = subject;
                mail.Body = message;
                if (IsBodyHtml == "Y")
                { mail.IsBodyHtml = true; }
                
                //if (!string.IsNullOrEmpty(attachFile))
                //    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(from, "!RWrW5UeS!");
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                mail.Dispose();
            }
            catch (Exception e)
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                string MessageText = "Fail to send email:" + e;
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            OutXML.Save(OutXMLPath);

        }
        //проверка электронной гарантии
        private void EwarrantyMethod(XmlDocument RecievedRequest, string OutXMLPath)
        {
            string SerialNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/PRODUCT/SERIALNO").InnerText;
            if (SerialNo.Length > 10 && SerialNo.Length < 19)  //Длина серийного номера от 11 до 20 символов 
            {
                string responseFromServer = EwarrantyRequest(SerialNo);
                if (responseFromServer == "")
                {
                    responseFromServer = EwarrantyRequest(SerialNo); //попробуем еще раз
                }
                // jQuery18307720148010850676_1489530389161({"Result":"OK","Record":{"Model":"SM-G935FZKUSER","SVCProduct":"THBB6","ValidFrom":null,"ValidTo":null,"WarrantyDuration":"12","SaleDate":"\/Date(1465160400000)\/","WarrantyTo":"\/Date(1496696400000)\/","WarrantyStart_str":"06.06.2016","WarrantyTo_str":"06.06.2017"},"Message":"Электронная гарантия найдена"});
                // jQuery18307720148010850676_1489530389161({"Result":"ERROR","Message":"Электронная гарантия не найдена"});
                // jQuery18307720148010850676_1489530389161({"Result":"OK","Record":{"Model":"WF60F1R2E2WDLP","SVCProduct":"SWM03","ValidFrom":null,"ValidTo":null,"WarrantyDuration":"12","SaleDate":"\/Date(1473282000000)\/","WarrantyTo":"\/Date(1504818000000)\/","WarrantyStart_str":"08.09.2016","WarrantyTo_str":"08.09.2017"},"Message":"Электронная гарантия найдена"});
                int Result = responseFromServer.IndexOf("Result\":\"OK");
                if (Result != -1) //"WarrantyStart_str":"08.09.2016"
                {
                    int WarrantyStartPosition = responseFromServer.IndexOf("WarrantyStart_str");
                    DateTime WarrantyStart = DateTime.Parse(responseFromServer.Substring(WarrantyStartPosition + 20, 10));
                    // тут записываем новый xml файл, его отправляем клиенту
                    CreateXMLFileForAnswer(OutXMLPath);
                    XmlDocument OutXML = new XmlDocument();
                    OutXML.Load(OutXMLPath);
                    WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    XmlNode BODY = OutXML.CreateElement("BODY");
                    OutXML.DocumentElement.AppendChild(BODY);
                    XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");
                    BODY.AppendChild(Response);
                    XmlNode EwarrantyStart = OutXML.CreateElement("EwarrantyStart");
                    EwarrantyStart.InnerText = WarrantyStart.ToString("yyyyMMdd");
                    Response.AppendChild(EwarrantyStart);
                    OutXML.Save(OutXMLPath);

                }
                else
                {
                    CreateXMLFileForAnswer(OutXMLPath);
                    XmlDocument OutXML = new XmlDocument();
                    OutXML.Load(OutXMLPath);
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    //добавить поле message в котором написать No ewarranty registred
                    OutXML.Save(OutXMLPath);


                }
            }
            else
            {
                CreateXMLFileForAnswer(OutXMLPath);
                XmlDocument OutXML = new XmlDocument();
                OutXML.Load(OutXMLPath);
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                //добавить поле message в котором написать SerialNo is incorrect
                OutXML.Save(OutXMLPath);
            }
        }
        //проверка статуса смс за период с сайта
        private void SMSList(XmlDocument RecievedRequest, string OutXMLPath)
        {
            string fromDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CRM_SMSLIST_REQUEST/fromDate").InnerText;
            string toDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CRM_SMSLIST_REQUEST/toDate").InnerText;
            string url = "https://lcab.smsintel.ru/";
            var ColloctionPhonesForAnswer = new List<string>();
            string BalanceFromSite = "1675.63";//на шару, если вдруг недоступен кабинет. заглушка

            try
            {
                HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
                tQ.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
                tQ.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                tQ.ContentType = "application/x-www-form-urlencoded";
                tQ.Referer = "https://lcab.smsintel.ru/send";
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                CookieCollection CookieC = tS.Cookies;
                //string tC = tS.Headers["Set-Cookie"];
                //Console.WriteLine(tS.Headers);
                tS.Close();

                string par = "ajax=1&p=%7B%22login%22%3A%2279145433714%22%2C%22password%22%3A%2204935152369%22%2C%22authCode%22%3A%22get%22%2C%22activationCode%22%3A%22%22%7D&returnDataType=json&";
                //string coc = "fsmgxt6vlappkmczuy6e";
                HttpWebRequest tQ1 = (HttpWebRequest)HttpWebRequest.Create("https://lcab.smsintel.ru/cmd/system/user/authCheck");
                tQ1.Method = "POST";
                tQ1.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
                tQ1.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                tQ1.ContentType = "application/x-www-form-urlencoded";
                tQ1.Referer = "https://lcab.smsintel.ru/";
                tQ1.CookieContainer = new CookieContainer();
                tQ1.CookieContainer.Add(CookieC);
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
                tQ1.ContentLength = bytes.Length;
                System.IO.Stream sendStream = tQ1.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS1 = (HttpWebResponse)tQ1.GetResponse();
                System.IO.Stream ReceiveStream = tS1.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                // string tC = tS.Headers["Set-Cookie"];
                string responseFromServer = sr.ReadToEnd();
                //Console.WriteLine(responseFromServer);
                CookieC = tS1.Cookies;
                tS1.Close();

                //par = "ajax=1&onPage=500&start=14.01.2017&stop=14.01.2017&phone=&source=&essence=sms&page=0&returnDataType=html";
                par = "ajax=1&onPage=500&start=" + fromDate + "&stop=" + toDate + "&phone=&source=&essence=sms&page=0&returnDataType=html";
                //string coc = "fsmgxt6vlappkmczuy6e";
                HttpWebRequest tQ2 = (HttpWebRequest)HttpWebRequest.Create("https://lcab.smsintel.ru/cmd/lcab/lcab_reports/smsListReport");
                tQ2.Method = "POST";
                tQ2.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
                tQ2.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                tQ2.ContentType = "application/x-www-form-urlencoded";
                tQ2.Referer = "https://lcab.smsintel.ru/send";
                tQ2.CookieContainer = new CookieContainer();
                tQ2.CookieContainer.Add(CookieC);
                bytes = System.Text.Encoding.ASCII.GetBytes(par);
                tQ2.ContentLength = bytes.Length;
                System.IO.Stream sendStream1 = tQ2.GetRequestStream();
                sendStream1.Write(bytes, 0, bytes.Length);
                sendStream1.Close();
                HttpWebResponse tS2 = (HttpWebResponse)tQ2.GetResponse();
                ReceiveStream = tS2.GetResponseStream();
                System.IO.StreamReader sr1 = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                // string tC = tS.Headers["Set-Cookie"];
                responseFromServer = sr1.ReadToEnd();
                //Console.WriteLine(responseFromServer);
                tS2.Close();

                // обработаем полученный HTML. Берем один <tr>.......</tr>, анализируем его и удаляем из строки ответа
                string GeneralTable = responseFromServer.Substring(responseFromServer.IndexOf("<tbody>"), responseFromServer.Length - responseFromServer.IndexOf("<tbody>"));
                String PhoneNo = "";
                while (GeneralTable.IndexOf("<tr>") > -1)
                {
                    string Cell = GeneralTable.Substring(GeneralTable.IndexOf("<tr>"), GeneralTable.IndexOf("</tr>") + 5 - GeneralTable.IndexOf("<tr>"));
                    if (Cell.IndexOf("Не доставлено") > 0 || Cell.IndexOf("Абонент заблокирован") > 0)
                    {
                        PhoneNo = Cell.Substring(Cell.IndexOf("')\">") + 5, 10);
                        ColloctionPhonesForAnswer.Add(PhoneNo);
                        GeneralTable = GeneralTable.Replace(Cell, "");
                    }
                    else
                    {
                        GeneralTable = GeneralTable.Replace(Cell, "");
                    }
                }

                url = "https://lcab.smsintel.ru/";
                tQ2 = (HttpWebRequest)HttpWebRequest.Create(url);
                tQ2.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
                tQ2.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                tQ2.ContentType = "application/x-www-form-urlencoded";
                tQ2.Referer = "https://www.smsintel.ru/";
                tQ2.CookieContainer = new CookieContainer();
                tQ2.CookieContainer.Add(CookieC);
                tS2 = (HttpWebResponse)tQ2.GetResponse();
                ReceiveStream = tS2.GetResponseStream();
                System.IO.StreamReader sr2 = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr2.ReadToEnd();
                tS2.Close();


                BalanceFromSite = responseFromServer.Substring(responseFromServer.IndexOf("accountUpdate\">") + 15, 7);
                BalanceFromSite = BalanceFromSite.Replace("<", "");
                BalanceFromSite = BalanceFromSite.Replace("/", "");
                BalanceFromSite = BalanceFromSite.Replace("s", "");

            }
            catch { }

            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");
            BODY.AppendChild(Response);
            XmlNode Balance = OutXML.CreateElement("balance");
            Balance.InnerText = BalanceFromSite;
            Response.AppendChild(Balance);
            foreach (var phone in ColloctionPhonesForAnswer)
            {
                XmlNode phoneNo = OutXML.CreateElement("phoneNo");
                phoneNo.InnerText = phone;
                Response.AppendChild(phoneNo);
            }
            OutXML.Save(OutXMLPath);

        }
        //получение информации о запчасти
        private void ADVPartsInfo(XmlDocument RecievedRequest, string OutXMLPath)
        {

            string partNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_PARTS_INFO_REQUEST/partNo").InnerText;
            //string toDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CRM_SMSLIST_REQUEST/toDate").InnerText;
            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            string responseFromServer = ADVPartsInfoRequest(partNo,false);

            while (responseFromServer.IndexOf("Your session has expired") != -1)
            {

                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageText = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = ADVPartsInfoRequest(partNo, false);
            }

            if (responseFromServer.IndexOf(partNo) == -1) //на случай если инфо о детали не найднео
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                OutXML.Save(OutXMLPath);
                return;
            }

            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode ResponseMAIN = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");
            //BODY.AppendChild(Response);

            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //вот этим превращаем в объект
            var rc = js.Deserialize<PartInfoClass>(responseFromServer);
            Dictionary<string, string> partInfo = MakePartDictinary(rc);
            foreach (var dc in partInfo)
                {
                    XmlNode node = OutXML.CreateElement(dc.Key);
                    node.InnerText = dc.Value;
                    ResponseMAIN.AppendChild(node);
                }
            BODY.AppendChild(ResponseMAIN);

            if (rc.ptAltmList.Count > 0)
            { 
                for (int i = 0; i < rc.ptAltmList.Count; i++)
                {
                    XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE_ALT");
                    Dictionary<string, string> partInfoALT = MakePartDictinaryALT(rc, i);
                    foreach (var dc in partInfoALT)
                    {
                        XmlNode node = OutXML.CreateElement(dc.Key);
                        node.InnerText = dc.Value;
                        Response.AppendChild(node);
                    }
                    ResponseMAIN.AppendChild(Response);
                }
            }

            OutXML.Save(OutXMLPath);

        }
        //получает серийный или имей и возвращает модель и срок гарнатии итд
        private void AllWtyInfoBySerialImei(XmlDocument RecievedRequest, string OutXMLPath)
        { 
            //алгоритм. Нам передают имей или серийник, надо вернуть и получить:
            //Серийный номер serialNo
            //ИМЕЙ IMEI
            //Модель modelNumber
            //Описание модели  model_desc
            //Версию versionNo
            //Тип продукта auth_gr
            //Дату продажи по электронной гарантии eWarty
            //Дату производства prodDate
            //Срок гарантии по дате производства wtyPeriodProduct
            //Срок гарантии по дате продажи  wtyPeriodPurch
            //Дату продажи из ключевых деталей keyPartsDate

            //формат запроса
            //<?xml version='1.0' encoding='UTF-8'?>
            //<MESSAGE>
            //<HEADER>
                //  <companyCode>C941</companyCode>
                //  <customerCode>0006056078</customerCode>
                //  <branchNo>0003427333</branchNo>
                //  <messageType>ALL_WTY_INFO_BY_SERIAL_IMEI</messageType>
                //  <messageKey>TEST1</messageKey>
                //  <extUser>TEST</extUser>
                //  <countryCode>RU</countryCode>
                //  <language>RU</language>
                //  <salesOrg>9E01</salesOrg>
                //  <headerType status="request" />
                //  <authKey></authKey>
            //</HEADER>
            //<BODY>
                //  <ALL_WTY_INFO_BY_SERIAL_IMEI_REQUEST>
                //   <serialNo>3709-001811</serialNo>
                //  </ALL_WTY_INFO_BY_SERIAL_IMEI_REQUEST>
            //</BODY>
            //</MESSAGE>
                                                        // формат ответа
                                                        //<?xml version="1.0" encoding="UTF-8"?>
                                                        //<MESSAGE>
                                                        //<HEADER>  
                                                            // <headerType status="ok"/>  
                                                            // <messageKey>С941_MD_ADDR_INFO_00000016563</messageKey> 
                                                            //  <messageType>ALL_WTY_INFO_BY_SERIAL_IMEI</messageType>    
                                                            //<client>polyservice</client>
                                                        // </HEADER> 
                                                        // <BODY>  
                                                        // <ALL_WTY_INFO_BY_SERIAL_IMEI_RESPONSE>  
                                                            //   <modelNumber>SM-G935FZKUSER</modelNumber>
                                                            //    <model_desc>MOBILE,SM-G935FD,BLACK,SER</model_desc>
                                                           //    <model_returnCode>1</model_returnCode>
                                                            //<model_country>1</model_country>
                                                            //   <serialNo>fdsfe123123</serialNo>
                                                            //   <versionNo>fdsfe123123</versionNo>
                                                            //    <imei>13123213123</imei>   
                                                            //   <prodDate>20170401</prodDate>
                                                            //   <eWarty>20170501</eWarty> 
                                                            //   <wtyPeriodProduct>12</wtyPeriodProduct>  
                                                            //   <wtyPeriodPurch>12</wtyPeriodPurch> 
                                                            //   <keyPartsDate></keyPartsDate>

                                                            //   <SPNo></SPNO>
                                                            //   <SPSerial></SPSerial>
                                                            //   <SPBalance></SPBalance>

                                                        // </ALL_WTY_INFO_BY_SERIAL_IMEI_RESPONSE>
                                                        //</BODY>
                                                        //</MESSAGE>
            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            string MessageText = "";

            string serialNo = "";
            string IMEI = "";
            string modelNumber = "";
            string model_country = "";
            string versionNo = "";
            string auth_gr = "";
            string model_desc = "";
            string model_returnCode = "";
            string eWarty = "";
            string prodDate = "";
            string wtyPeriodProduct = "";
            string wtyPeriodPurch = "";
            string keyPartsDate = "";
            string wtyPeriodKeyPart = "";
            string keyPartsRepairDate = "";
            string SPNo = "";
            string SPSerial = "";
            string SPBalance = "";
            string SPCharge = "";
            string SPExpire = "";
            char splitter = '|';

            bool HHP = false;

            string EXPar = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ALL_WTY_INFO_BY_SERIAL_IMEI_REQUEST/serialNo").InnerText;
            string MOTP = "";
            try
            {
                MOTP = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/mOtp").InnerText;
            }
            catch { }

            string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int i = 0;
            while (i <= EXPar.Length)
            {
                if (EXPar.IndexOfAny(alphabet.ToCharArray(), i) >= 0)
                {
                    serialNo = EXPar;
                }
                ++i;
            }

            if (serialNo == "")
            { IMEI = EXPar;
                if(IMEI.Length != 15)
                { 
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    MessageText = "Check IMEI";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                HHP = true;
            }
            else
            {
                if (serialNo.Length < 11 && serialNo.Length > 22)
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    MessageText = "Check serial no";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                if (serialNo.Length == 11)
                {
                    HHP = true;
                }
            }


            AddToLogFile("AllWtyInfoBySerialImei начинаем для " + EXPar);

            string responseFromServer = "";
            string subResponseFromServer = "";
            if (HHP)//проверим на РСТ и вернем еще раз модель
            {
                responseFromServer = GetModelBySerialANDDummyModelRequest(EXPar, MOTP);
                while (responseFromServer == "" || responseFromServer.IndexOf("\"returnCode\":\"") == -1)
                {
                    AddToLogFile("GetModelBySerialANDDummyModelRequest(EXPar);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetModelBySerialANDDummyModelRequest(EXPar, MOTP);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                subResponseFromServer = subResponseFromServer.Replace("{", "");
                subResponseFromServer = subResponseFromServer.Replace("}", "");

                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name == "returnCode")
                       if(value == "N")
                        {
                            WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                            MessageText = "There is no information for this parametr";
                            AddMessageToHeader(OutXML, MessageText);
                            OutXML.Save(OutXMLPath);
                            return;
                        }
                        else
                        {
                            if (value == "M")
                            {
                                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                                MessageText = "Wrong master OTP";
                                AddMessageToHeader(OutXML, MessageText);
                                OutXML.Save(OutXMLPath);
                                return;
                            }
                        }
                    if (name == "model") { modelNumber = value; }
                    if (name == "overseas")
                    {
                        if (value != "N")
                        {
                            model_returnCode = "1";
                        }
                        else
                        {
                            model_returnCode = "0";
                        }
                    }
                    if (name == "country")
                    {

                        model_country = value;

                    }
                    if (name == "serial")
                    {

                        serialNo = value;

                    }
                    
                }
            }

            //получим модель версию по серийному номеру или имей // <model>SM-G935FZKUSER</model><verNo>fdsfe123123</verNo>
            responseFromServer = GetModelBySerialRequest(EXPar);
            while (responseFromServer == "" || responseFromServer.IndexOf("<html>") != -1)
            {
                AddToLogFile("GetModelBySerialRequest(EXPar);responseFromServer пришел пустой");
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageTextError = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageTextError);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                
                
                //CheckGSPNAuthorization();
                responseFromServer = GetModelBySerialRequest(EXPar);
            }
            //while (responseFromServer.IndexOf("<html>") != -1) 
            //{ 
            //    GetGSPNCoockies();
            //    responseFromServer = GetModelBySerialRequest(EXPar);
            //}
            //AddCookie();
            subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
            subResponseFromServer = subResponseFromServer.Replace("{", "");
            subResponseFromServer = subResponseFromServer.Replace("}", "");
            foreach (string parametr in subResponseFromServer.Split(splitter))
            {
                string Sparametr = parametr.Replace("\"", "");
                string name = Sparametr.Split(':')[0];
                string value = Sparametr.Substring(name.Length + 1);
                if (name == "modelNumber" && modelNumber == "")
                {
                    modelNumber = value;
                }
                else
                {
                    if (name == "versionNo")
                    {
                        versionNo = value;
                    }
                }
            }
            
           

            // получим //    <modelDesc>MOBILE,SM-G935FD,BLACK,SER</modelDesc> и вспомогательный тип продукта
            if (modelNumber != "")//если модель была получена
            {
                responseFromServer = GetModelDescription(modelNumber);
                while (responseFromServer == "")
                {
                    AddToLogFile("GetModelDescription(modelNumber);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetModelDescription(modelNumber);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name.IndexOf("auth_gr") > -1) { auth_gr = value; }
                    else 
                    {
                        if (name.IndexOf("model_desc") > -1)
                        {
                            model_desc = value;
                        }
                        else
                        {
                            if (name.IndexOf("returnCode") > -1)
                            {
                                if (model_returnCode == "")//могли получить раньше. На гспн каша. Модель может быть не русской но зарегистрированной в трекинге. Проверяем и там и там
                                {
                                    model_returnCode = value;
                                }
                            }
                        }
                    }
                }
            }
            
            //если есть имей (модель тогда точно есть) то получим серийный номер//   <serialNo>fdsfe123123</serialNo>
           if (serialNo == "")//по идее вообще сюда не должен заходить
            {
                AddToLogFile("по идее вообще сюда не должен заходить");
                responseFromServer = GetSNByIMEI(modelNumber, IMEI, MOTP);
                while (responseFromServer == "" || responseFromServer.IndexOf("returnCode\":\"") == -1)
                {
                    AddToLogFile("GetSNByIMEI(modelNumber, IMEI);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetSNByIMEI(modelNumber, IMEI, MOTP);
                }

                //if (responseFromServer.IndexOf("no Data in MPTS"
                serialNo = responseFromServer.Substring(responseFromServer.IndexOf("serial\":\"") + 9, 11);
                //serialNo = serialNo.Substring(0, serialNo.IndexOf("\",\"pr"));
                //if (serialNo.IndexOf("\"") != -1) { serialNo = ""; }
           }
           // model_returnCode = "0";
            //получим электронную гарантию //   <eWarty>20170501</eWarty> 
           if (model_returnCode == "0")//аппарат РСТ
           {
               if (IMEI == "")
               {
                   responseFromServer = EwarrantyRequest(serialNo);
                   if (responseFromServer == "")
                   {
                       responseFromServer = EwarrantyRequest(serialNo); //попробуем еще раз
                   }
               }
               else
               {
                   responseFromServer = EwarrantyRequest(IMEI);
                   if (responseFromServer == "")
                   {
                       responseFromServer = EwarrantyRequest(IMEI); //попробуем еще раз
                   }
               }

               int Result = responseFromServer.IndexOf("Result\":\"OK");
               if (Result != -1) //"WarrantyStart_str":"08.09.2016"
               {
                   DateTime eWartyDate = DateTime.Parse(responseFromServer.Substring(responseFromServer.IndexOf("WarrantyStart_str") + 20, 10));
                   eWarty = eWartyDate.ToString("dd.MM.yyyy");
               }
           }

            //получим срок гарантии по дате производства и по электронной дате продажи при наличии и дату производства <prodDate>20170401</prodDate> <wtyPeriod>12</wtyPeriod>
            if (modelNumber != "" & model_returnCode == "0")//если модель была получена и она РСТ
            { 
               // срок гарантии по дате прозводства
                responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, prodDate, auth_gr, IMEI);
                while (responseFromServer == "")
                {
                    AddToLogFile("GetWarrantyPeriod(modelNumber, serialNo, prodDate, auth_gr, IMEI);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, prodDate, auth_gr, IMEI);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                subResponseFromServer.Replace("{", "");
                subResponseFromServer.Replace("}", "");
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name == "partsTermC4") { wtyPeriodProduct = value; }
                    else 
                        { if (name == "prod_date") 
                            { if (value != "00000000")
                                {
                                    prodDate = DateTime.Parse(value).ToString("dd.MM.yyyy");
                                }
                                else
                                {
                                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                                    MessageText = "Cant parse production date. Check serial number";
                                    AddMessageToHeader(OutXML, MessageText);
                                    OutXML.Save(OutXMLPath);
                                    return;
                                }   
                            }
                        }
                }
                if (wtyPeriodProduct == "")
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    MessageText = "There is no warranty period for this serial no. Something wrong on GSPN";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                // срок гарантии по дате электронной гарантии
                if (eWarty != "")
                {
                    responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, eWarty, auth_gr, IMEI);
                    while (responseFromServer == "")
                    {
                        AddToLogFile("GetWarrantyPeriod(modelNumber, serialNo, eWarty, auth_gr, IMEI);;responseFromServer пришел пустой");
                        if (!CheckGSPNAuthorization())
                        {
                            WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                            string MessageTextError = "GSPN authorization fail";
                            AddMessageToHeader(OutXML, MessageTextError);
                            OutXML.Save(OutXMLPath);
                            return;
                        }
                        responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, eWarty, auth_gr, IMEI);
                    }
                    subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                    subResponseFromServer.Replace("{", "");
                    subResponseFromServer.Replace("}", "");
                    foreach (string parametr in subResponseFromServer.Split(splitter))
                    {
                        string Sparametr = parametr.Replace("\"", "");
                        string name = Sparametr.Split(':')[0];
                        string value = Sparametr.Substring(name.Length + 1);
                        if (name == "laborterm") { wtyPeriodPurch = value; }
                    }
                }
            }

           // узнаем дату ремонта в ключевых деталях//   <keyPartsDate></keyPartsDate> 
            if (model_returnCode == "0" & modelNumber != "" & serialNo != "")
            {
                responseFromServer = NEWGetKeyInfo(modelNumber, serialNo, IMEI);
                while (responseFromServer == "")
                {
                    AddToLogFile("NEWGetKeyInfo(serialNo);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = NEWGetKeyInfo(modelNumber, serialNo, IMEI);
                }
                if (responseFromServer != "ThereIsNoEnoughParameters")
                {

                    var js1 = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //вот этим превращаем в объект
                    var rc1 = new RootKeyPartsInfo();

                    try { rc1 = js1.Deserialize<RootKeyPartsInfo>(responseFromServer); }

                    catch (Exception e) { AddToLogFile("NEWGetKeyInfo(serialNo) не удалось десериализовать " + e); }

                    if (rc1 != null)
                    { 
                        if (rc1.success & !rc1.error)
                        {

                            //rc1.ptWdata.Sort((x, y) => x.comp_dt.CompareTo(y.comp_dt));
                            //rc1.ptWdata.Sort((x, y) => x.comp_dt.(y.comp_dt));
                            foreach (ptWdata repair in rc1.ptWdata)//rc.PtDataList)
                            {
                                if (repair.sub_term == "I")
                                {
                                    if (repair.pur_dt == "00.00.0000")
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (keyPartsDate == "")
                                        {
                                            keyPartsDate = DateTime.Parse(repair.pur_dt).ToString("dd.MM.yyyy");
                                        }
                                        keyPartsRepairDate = DateTime.Parse(repair.comp_dt).ToString("dd.MM.yyyy");
                                    }
                                }

                            }
                        }
                    }

                    //var count1 = responseFromServer.IndexOf("ptDataList\":[");
                    //var count2 = responseFromServer.Length;
                    //var count3 = responseFromServer.IndexOf("]") + 13 - count1;
                    //subResponseFromServer = responseFromServer.Substring(count1 + 13, responseFromServer.LastIndexOf("]") - 13 - count1);
                    //foreach (string globalparametr in subResponseFromServer.Split('}'))
                    //{
                    //    string SubPar = globalparametr.Replace("\",\"", splitter.ToString());
                    //    if (SubPar.Length < 20) { continue; }

                    //    foreach (string parametr in SubPar.Split(splitter))
                    //    {
                    //        string Sparametr = parametr.Replace("\"", "");
                    //        string name = Sparametr.Split(':')[0];
                    //        string value = Sparametr.Substring(name.Length + 1);
                    //        if (name == "hqTerm")
                    //        {
                    //            if (value != "I")
                    //            { break; }
                    //        }
                    //        if (name == "purDate") { if (value == "00.00.0000") { continue; } else { keyPartsDate = DateTime.Parse(value).ToString("dd.MM.yyyy"); } }
                    //    }
                    //}
                }
            }

            if (keyPartsDate != "" & modelNumber != "")
            {
                responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, keyPartsDate, auth_gr, IMEI);
                while (responseFromServer == "")
                {
                    AddToLogFile("GetWarrantyPeriod(modelNumber, serialNo, keyPartsDate, auth_gr, IMEI);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, keyPartsDate, auth_gr, IMEI);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                subResponseFromServer.Replace("{", "");
                subResponseFromServer.Replace("}", "");
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name == "laborterm") { wtyPeriodKeyPart = value; }
                }
            }

            if (IMEI != "" & serialNo != "")
            {
                responseFromServer = GetADHInfo(serialNo, IMEI);
                while (responseFromServer == "")
                {
                    AddToLogFile("GetADHInfo(serialNo, IMEI);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetADHInfo(serialNo, IMEI);
                }

                var js = new System.Web.Script.Serialization.JavaScriptSerializer();
                //вот этим превращаем в объект
                var rc = js.Deserialize<RootClassGetADH>(responseFromServer);
            
                if(rc.success)
                {
                    if(rc.eS_RESULT.MODEL == modelNumber & rc.eS_RESULT.SERNO == serialNo)
                    {
                        
                        SPNo = rc.eS_RESULT.PACNO;
                        SPSerial = rc.eS_RESULT.CERTI;
                        SPBalance = rc.eS_RESULT.BALANCE;
                        SPCharge = rc.eS_RESULT.CHARGE_AMOUNT;
                        SPExpire = rc.eS_RESULT.e_DAT;
                    }
                }
            
            
            }



            // формат ответа
            //<?xml version="1.0" encoding="UTF-8"?>
            //<MESSAGE>
            //<HEADER>  
            // <headerType status="ok"/>  
            // <messageKey>С941_MD_ADDR_INFO_00000016563</messageKey> 
            //  <messageType>ALL_WTY_INFO_BY_SERIAL_IMEI</messageType>    
            //<client>polyservice</client>
            // </HEADER> 
            // <BODY>  
            // <ALL_WTY_INFO_BY_SERIAL_IMEI_RESPONSE>  
            //   <modelNumber>SM-G935FZKUSER</modelNumber>
            //    <model_desc>MOBILE,SM-G935FD,BLACK,SER</model_desc>  
            //    <model_returnCode>0</model_returnCode>
            //   <serialNo>fdsfe123123</serialNo>
            //   <versionNo>fdsfe123123</versionNo>
            //    <imei>13123213123</imei>   
            //   <prodDate>20170401</prodDate>
            //   <eWarty>20170501</eWarty> 
            //   <wtyPeriodProduct>12</wtyPeriodProduct>  
            //   <wtyPeriodPurch>12</wtyPeriodPurch> 
            //   <keyPartsDate></keyPartsDate>
            // </ALL_WTY_INFO_BY_SERIAL_IMEI_RESPONSE>
            //</BODY>
            //</MESSAGE>

         
            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");
            
            XmlNode node = OutXML.CreateElement("modelNumber");
            node.InnerText = modelNumber;
            Response.AppendChild(node);

            node = OutXML.CreateElement("model_desc");
            node.InnerText = model_desc;
            Response.AppendChild(node);

            node = OutXML.CreateElement("model_returnCode");
            node.InnerText = model_returnCode;
            Response.AppendChild(node);

            node = OutXML.CreateElement("model_country");
            node.InnerText = model_country;
            Response.AppendChild(node); 

            node = OutXML.CreateElement("productType");
            node.InnerText = auth_gr;
            Response.AppendChild(node); 

            node = OutXML.CreateElement("serialNo");
            node.InnerText = serialNo;
            Response.AppendChild(node);

            node = OutXML.CreateElement("versionNo");
            node.InnerText = versionNo;
            Response.AppendChild(node);

            node = OutXML.CreateElement("imei");
            node.InnerText = IMEI;
            Response.AppendChild(node);

            node = OutXML.CreateElement("prodDate");
            node.InnerText = prodDate;
            Response.AppendChild(node);
          
            node = OutXML.CreateElement("eWarty");
            node.InnerText = eWarty;
            Response.AppendChild(node);

            node = OutXML.CreateElement("wtyPeriodProduct");
            node.InnerText = wtyPeriodProduct;
            Response.AppendChild(node);
            
            node = OutXML.CreateElement("wtyPeriodPurch");
            node.InnerText = wtyPeriodPurch;
            Response.AppendChild(node);

            node = OutXML.CreateElement("keyPartsDate");
            node.InnerText = keyPartsDate;
            Response.AppendChild(node);
           
            node = OutXML.CreateElement("keyPartsRepairDate");
            node.InnerText = keyPartsRepairDate;
            Response.AppendChild(node);

            node = OutXML.CreateElement("wtyPeriodKeyPart");
            node.InnerText = wtyPeriodKeyPart;
            Response.AppendChild(node);
            
            node = OutXML.CreateElement("SPNo");
            node.InnerText = SPNo;
            Response.AppendChild(node);

            node = OutXML.CreateElement("SPSerial");
            node.InnerText = SPSerial;
            Response.AppendChild(node);

            node = OutXML.CreateElement("SPBalance");
            node.InnerText = SPBalance;
            Response.AppendChild(node);

            node = OutXML.CreateElement("SPCharge");
            node.InnerText = SPCharge;
            Response.AppendChild(node);

            node = OutXML.CreateElement("SPExpire");
            node.InnerText = SPExpire;
            Response.AppendChild(node); 

            BODY.AppendChild(Response);

            OutXML.Save(OutXMLPath);
           
            



        }
        //получает модель и возвращает описание модели
        private void GetModelInfo(XmlDocument RecievedRequest, string OutXMLPath)
        {
            //алгоритм. Нам передают модель и, надо вернуть ее описание и принадлежность к РСТ
            
            //Модель modelNumber
            //Описание модели  model_desc
            

            //формат запроса
            //<?xml version='1.0' encoding='UTF-8'?>
            //<MESSAGE>
            //<HEADER>
            //  <companyCode>C941</companyCode>
            //  <customerCode>0006056078</customerCode>
            //  <branchNo>0003427333</branchNo>
            //  <messageType>GET_MODEL_INFO</messageType>
            //  <messageKey>TEST1</messageKey>
            //  <extUser>TEST</extUser>
            //  <countryCode>RU</countryCode>
            //  <language>RU</language>
            //  <salesOrg>9E01</salesOrg>
            //  <headerType status="request" />
            //  <authKey></authKey>
            //</HEADER>
            //<BODY>
            //  <GET_MODEL_INFO_REQUEST>
            //   <modelNumber>3709-001811</modelNumber>
            //  </GET_MODEL_INFO_REQUEST>
            //</BODY>
            //</MESSAGE>
                                            // формат ответа
                                            //<?xml version="1.0" encoding="UTF-8"?>
                                            //<MESSAGE>
                                            //<HEADER>  
                                            // <headerType status="ok"/>  
                                            // <messageKey>С941_MD_ADDR_INFO_00000016563</messageKey> 
                                            //  <messageType>GET_MODEL_INFO</messageType>    
                                            //<client>polyservice</client>
                                            // </HEADER> 
                                            // <BODY>  
                                            // <GET_MODEL_INFO_RESPONSE>  
                                            //   <modelNumber>SM-G935FZKUSER</modelNumber>
                                            //   <model_desc>MOBILE,SM-G935FD,BLACK,SER</model_desc>
                                            //     <productType>MOBILE,SM-G935FD,BLACK,SER</productType>//auth_gr
                                            //   <model_returnCode>MOBILE,SM-G935FD,BLACK,SER</model_returnCode>
                                            // </GET_MODEL_INFO_RESPONSE>
                                            //</BODY>
                                            //</MESSAGE>
            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            string MessageText = "";


            string modelNumber = "";
            string auth_gr = "";
            string model_desc = "";
            string model_returnCode = "";
            string hqSvcProd = "";

            char splitter = '|';

            modelNumber = RecievedRequest.SelectSingleNode("MESSAGE/BODY/GET_MODEL_INFO_REQUEST/modelNumber").InnerText;

            string alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзиклмнопрстуфхцчшщъыьэюя";
            int i = 0;
            while (i <= modelNumber.Length)
            {
                if (modelNumber.IndexOfAny(alphabet.ToCharArray(), i) >= 0)
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    MessageText = "No ewarranty registred";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                ++i;
            }

            if (modelNumber != "")//если модель была получена
            {
                string responseFromServer = GetModelDescription(modelNumber);
                while (responseFromServer == "")
                {
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetModelDescription(modelNumber);
                }
                string subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name.IndexOf("auth_gr") > -1) { auth_gr = value; }
                    else
                    {
                        if (name.IndexOf("model_desc") > -1)
                        {
                            model_desc = value;
                        }
                        else
                        {
                            if (name.IndexOf("returnCode") > -1)
                            {
                                model_returnCode = value;
                            }
                            else
                            {
                                if (name.IndexOf("hq_svc_prod") > -1)
                                {
                                    hqSvcProd = value;
                                }
                            }
                        }
                    }
                }
            }

            if (auth_gr == "" & model_desc == "")
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Cant recive some arguments. Check sended paremeters";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

            XmlNode node = OutXML.CreateElement("modelNumber");
            node.InnerText = modelNumber;
            Response.AppendChild(node);

            node = OutXML.CreateElement("model_desc");
            node.InnerText = model_desc;
            Response.AppendChild(node);

            node = OutXML.CreateElement("model_returnCode");
            node.InnerText = model_returnCode;
            Response.AppendChild(node); 

            node = OutXML.CreateElement("productType");
            node.InnerText = auth_gr;
            Response.AppendChild(node);

            node = OutXML.CreateElement("hqSvcProd");
            node.InnerText = hqSvcProd;
            Response.AppendChild(node);

            BODY.AppendChild(Response);

            OutXML.Save(OutXMLPath);
        
        }

        private void GetWtyInfoByModelSerial(XmlDocument RecievedRequest, string OutXMLPath)
        {
            //алгоритм. Нам передают модель, серийник, IMEI, тип продукта(опционально), дата продажи(опционально) надо вернуть

            
            //Версию versionNo
            //Дату производства prodDate
            //Срок гарантии по дате производства wtyPeriodProduct
            //Срок гарантии по дате продажи  wtyPeriodPurch


            //формат запроса
            //<?xml version='1.0' encoding='UTF-8'?>
            //<MESSAGE>
            //<HEADER>
            //  <companyCode>C941</companyCode>
            //  <customerCode>0006056078</customerCode>
            //  <branchNo>0003427333</branchNo>
            //  <messageType>GET_WTY_INFO_BY_MODEL_SERIAL</messageType>
            //  <messageKey>TEST1</messageKey>
            //  <extUser>TEST</extUser>
            //  <countryCode>RU</countryCode>
            //  <language>RU</language>
            //  <salesOrg>9E01</salesOrg>
            //  <headerType status="request" />
            //  <authKey></authKey>
            //</HEADER>
            //<BODY>
            //  <>GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST>
            //  <modelNumber>SM-G935FZKUSER</modelNumber>
            //   <serialNo>fdsfe123123</serialNo>
            //    <imei>13123213123</imei> 
            //    <productType>13123213123</productType>
            //     <purchDate>13123213123</purchDate>
            //  </>GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST>
            //</BODY>
            //</MESSAGE>
                                                    // формат ответа
                                                    //<?xml version="1.0" encoding="UTF-8"?>
                                                    //<MESSAGE>
                                                    //<HEADER>  
                                                    // <headerType status="ok"/>  
                                                    // <messageKey>С941_MD_ADDR_INFO_00000016563</messageKey> 
                                                     //  <messageType>>GET_WTY_INFO_BY_MODEL_SERIAL</messageType>    
                                                    //<client>polyservice</client>
                                                    // </HEADER> 
                                                    // <BODY>  
                                                        // <>GET_WTY_INFO_BY_MODEL_SERIAL_RESPONSE>
                                                    //   <prodDate>20170401</prodDate>
                                                     //  <wtyPeriodProduct>12</wtyPeriodProduct>  
                                                    //   <wtyPeriodPurch>12</wtyPeriodPurch> 
                                                    //</BODY>
                                                    //</MESSAGE>
            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            string MessageText = "";

            string modelNumber = RecievedRequest.SelectSingleNode("MESSAGE/BODY/GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST/modelNumber").InnerText;
            string serialNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST/serialNo").InnerText;
            string IMEI = RecievedRequest.SelectSingleNode("MESSAGE/BODY/GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST/imei").InnerText;
            string auth_gr = RecievedRequest.SelectSingleNode("MESSAGE/BODY/GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST/productType").InnerText;
            string purchDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/GET_WTY_INFO_BY_MODEL_SERIAL_REQUEST/purchDate").InnerText;
            string prodDate = "";
            string wtyPeriodProduct = "";
            string wtyPeriodPurch = "";
            string keyPartsDate = "";
            string wtyPeriodKeyPart = "";
            string eWarty = "";
            string wtyPeriodeWarty = "";
            string keyPartsRepairDate = "";


            char splitter = '|';

            if (modelNumber == "" || serialNo == "" || auth_gr == "")
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Some argument are empty. Check arguments";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            //получим срок гарантии по дате производства и по электронной дате продажи при наличии и дату производства <prodDate>20170401</prodDate> <wtyPeriod>12</wtyPeriod>

            // срок гарантии по дате прозводства. Просто пустую дату отправляем
            string responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, prodDate, auth_gr, IMEI);
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageTextError = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageTextError);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, prodDate, auth_gr, IMEI);
            }
            string subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
            subResponseFromServer.Replace("{", "");
            subResponseFromServer.Replace("}", "");
            foreach (string parametr in subResponseFromServer.Split(splitter))
            {
                string Sparametr = parametr.Replace("\"", "");
                string name = Sparametr.Split(':')[0];
                string value = Sparametr.Substring(name.Length + 1);
                if (name == "partsTermC4") { wtyPeriodProduct = value; }
                else
                {
                    if (name == "prod_date")
                    {
                        if (value != "00000000")
                        {
                            prodDate = DateTime.Parse(value).ToString("dd.MM.yyyy");
                        }
                        else
                        {
                            WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                            MessageText = "Cant parse production date. Check serial number";
                            AddMessageToHeader(OutXML, MessageText);
                            OutXML.Save(OutXMLPath);
                            return;
                        }
                    }
                }
            }


            if (IMEI == "")
            {
                responseFromServer = EwarrantyRequest(serialNo);
                if (responseFromServer == "")
                {
                    responseFromServer = EwarrantyRequest(serialNo); //попробуем еще раз
                }
            }
            else
            {
                responseFromServer = EwarrantyRequest(IMEI);
                if (responseFromServer == "")
                {
                    responseFromServer = EwarrantyRequest(IMEI); //попробуем еще раз
                }
            }

            int Result = responseFromServer.IndexOf("Result\":\"OK");
            if (Result != -1) //"WarrantyStart_str":"08.09.2016"
            {
                DateTime eWartyDate = DateTime.Parse(responseFromServer.Substring(responseFromServer.IndexOf("WarrantyStart_str") + 20, 10));
                eWarty = eWartyDate.ToString("dd.MM.yyyy");
            }



            // срок гарантии по дате электронной гарантии
            if (eWarty != "")
            {
                responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, eWarty, auth_gr, IMEI);
                while (responseFromServer == "")
                {
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, eWarty, auth_gr, IMEI);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                subResponseFromServer.Replace("{", "");
                subResponseFromServer.Replace("}", "");
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name == "laborterm") { wtyPeriodeWarty = value; }
                }
            }

            if (purchDate != "")
            {
                responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, purchDate, auth_gr, IMEI);
                while (responseFromServer == "")
                {
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, purchDate, auth_gr, IMEI);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                subResponseFromServer.Replace("{", "");
                subResponseFromServer.Replace("}", "");
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name == "laborterm") { wtyPeriodPurch = value; }
                }
            }



            //проверим ключевые детали
            if (modelNumber != "" & serialNo != "")
            {
                responseFromServer = NEWGetKeyInfo(modelNumber, serialNo, IMEI);
                while (responseFromServer == "")
                {
                    AddToLogFile("NEWGetKeyInfo(serialNo);responseFromServer пришел пустой");
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = NEWGetKeyInfo(modelNumber, serialNo, IMEI);
                }
                if (responseFromServer != "ThereIsNoEnoughParameters")
                {

                    var js1 = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //вот этим превращаем в объект
                    var rc1 = new RootKeyPartsInfo();

                    try { rc1 = js1.Deserialize<RootKeyPartsInfo>(responseFromServer); }

                    catch (Exception e) { AddToLogFile("NEWGetKeyInfo(serialNo) не удалось десериализовать " + e); }

                    if (rc1 != null)
                    {
                        if (rc1.success & !rc1.error)
                        {

                            // rc1.ptWdata.Sort((x, y) => x.comp_dt.CompareTo(y.comp_dt));

                            foreach (ptWdata repair in rc1.ptWdata)//rc.PtDataList)
                            {
                                if (repair.sub_term == "I")
                                {
                                    if (repair.pur_dt == "00.00.0000")
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (keyPartsDate == "")
                                        {
                                            keyPartsDate = DateTime.Parse(repair.pur_dt).ToString("dd.MM.yyyy");
                                        }
                                        keyPartsRepairDate = DateTime.Parse(repair.comp_dt).ToString("dd.MM.yyyy");
                                    }
                                }

                            }
                        }
                    }
                }
            }

                    //responseFromServer = GetKeyInfo(serialNo); старый метод. больше не работает на гспн
                    //while (responseFromServer == "")
                    //{
                    //    if (!CheckGSPNAuthorization())
                    //    {
                    //        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    //        OutXML.Save(OutXMLPath);
                    //        return;
                    //    }
                    //    responseFromServer = GetKeyInfo(serialNo);
                    //}
                    //var count1 = responseFromServer.IndexOf("ptDataList\":[");
                    //var count2 = responseFromServer.Length;
                    //var count3 = responseFromServer.IndexOf("]") + 13 - count1;
                    //subResponseFromServer = responseFromServer.Substring(count1 + 13, responseFromServer.LastIndexOf("]") - 13 - count1);
                    //foreach (string globalparametr in subResponseFromServer.Split('}'))
                    //{
                    //    string SubPar = globalparametr.Replace("\",\"", splitter.ToString());
                    //    if (SubPar.Length < 20) { continue; }

                    //    foreach (string parametr in SubPar.Split(splitter))
                    //    {
                    //        string Sparametr = parametr.Replace("\"", "");
                    //        string name = Sparametr.Split(':')[0];
                    //        string value = Sparametr.Substring(name.Length + 1);
                    //        if (name == "hqTerm")
                    //        {
                    //            if (value != "I")
                    //            { break; }
                    //        }
                    //        if (name == "purDate") { if (value == "00.00.0000") { continue; } else { keyPartsDate = DateTime.Parse(value).ToString("dd.MM.yyyy"); } }
                    //    }
                    //}

            if (keyPartsDate != "" & modelNumber != "")
            {
                responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, keyPartsDate, auth_gr, IMEI);
                while (responseFromServer == "")
                {
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = GetWarrantyPeriod(modelNumber, serialNo, keyPartsDate, auth_gr, IMEI);
                }
                subResponseFromServer = responseFromServer.Replace("\",\"", splitter.ToString());
                subResponseFromServer.Replace("{", "");
                subResponseFromServer.Replace("}", "");
                foreach (string parametr in subResponseFromServer.Split(splitter))
                {
                    string Sparametr = parametr.Replace("\"", "");
                    string name = Sparametr.Split(':')[0];
                    string value = Sparametr.Substring(name.Length + 1);
                    if (name == "laborterm") { wtyPeriodKeyPart = value; }
                }
            }


            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

            XmlNode node = OutXML.CreateElement("prodDate");
            node.InnerText = prodDate;
            Response.AppendChild(node);

            node = OutXML.CreateElement("wtyPeriodProduct");
            node.InnerText = wtyPeriodProduct;
            Response.AppendChild(node);

            node = OutXML.CreateElement("eWarty");
            node.InnerText = eWarty;
            Response.AppendChild(node);

            node = OutXML.CreateElement("wtyPeriodeWarty");
            node.InnerText = wtyPeriodeWarty;
            Response.AppendChild(node);

            node = OutXML.CreateElement("wtyPeriodPurch");
            node.InnerText = wtyPeriodPurch;
            Response.AppendChild(node);

            node = OutXML.CreateElement("keyPartsDate");
            node.InnerText = keyPartsDate;
            Response.AppendChild(node);

            node = OutXML.CreateElement("wtyPeriodKeyPart");
            node.InnerText = wtyPeriodKeyPart;
            Response.AppendChild(node);

            node = OutXML.CreateElement("keyPartsRepairDate");
            node.InnerText = keyPartsRepairDate;
            Response.AppendChild(node);

            BODY.AppendChild(Response);

            OutXML.Save(OutXMLPath);
        
        }

        private void ChangeGSPNCustomer(XmlDocument RecievedRequest, string OutXMLPath)
        {
            //алгоритм. Нам передают данные. Надо изменить клиента на ГСПН

            //формат запроса
            //<?xml version='1.0' encoding='UTF-8'?>
            //<MESSAGE>
            //<HEADER>
            //  <companyCode>C941</companyCode>
            //  <customerCode>0006056078</customerCode>
            //  <branchNo>0003427333</branchNo>
            //  <messageType>CHANGE_GSPN_CUSTOMER</messageType>
            //  <messageKey>TEST1</messageKey>
            //  <extUser>TEST</extUser>
            //  <countryCode>RU</countryCode>
            //  <language>RU</language>
            //  <salesOrg>9E01</salesOrg>
            //  <headerType status="request" />
            //  <authKey></authKey>
            //</HEADER>
            //<BODY>
            //  <CHANGE_GSPN_CUSTOMER_REQUEST>
            //   <NAME_FIRST>3709-001811</NAME_FIRST>
            //   <NAME_LAST>3709-001811</NAME_LAST>
            //   <CONSUMER>3709-001811</CONSUMER>
            //   <PHONE>3709-001811</PHONE>
            //   <STREET1>3709-001811</STREET1>
            //   <STREET2>3709-001811</STREET2>
            //   <STREET3>3709-001811</STREET3>
            //   <CITY>3709-001811</CITY>
            //   <REGION>3709-001811</REGION>
            //   <REGION_CODE>3709-001811</REGION_CODE>
            //   <POST_CODE>3709-001811</POST_CODE>
            //   <CITY_CODE>3709-001811</CITY_CODE>
            //</CHANGE_GSPN_CUSTOMER_REQUEST>
            //</BODY>
            //</MESSAGE>
                                                        // формат ответа
                                                        //<?xml version="1.0" encoding="UTF-8"?>
                                                        //<MESSAGE>
                                                        //<HEADER>  
                                                        // <headerType status="ok"/> или fail  
                                                        // <messageKey>С941_MD_ADDR_INFO_00000016563</messageKey> 
                                                        //  <messageType>CHANGE_GSPN_CUSTOMERCHANGE_GSPN_CUSTOMER</messageType>    
                                                        //<client>polyservice</client>
                                                        // </HEADER> 
                                                        // <BODY>  
                                                        //</BODY>
                                                        //</MESSAGE>
            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            //System.IO.StreamReader sr = new System.IO.StreamReader(RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/NAME_FIRST").InnerText, Encoding.UTF8);
            //string NAME_FIRST  = sr.ReadToEnd();
            //sr = new System.IO.StreamReader(RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/NAME_LAST").InnerText, Encoding.UTF8);
            //string NAME_LAST = sr.ReadToEnd();
            //string CONSUMER = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/CONSUMER").InnerText;
            //string PHONE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/PHONE").InnerText;
            //sr = new System.IO.StreamReader(RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/STREET1").InnerText, Encoding.UTF8);
            //string STREET1 = sr.ReadToEnd();
            //sr = new System.IO.StreamReader(RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/STREET2").InnerText, Encoding.UTF8);
            //string STREET2 = sr.ReadToEnd();
            //sr = new System.IO.StreamReader(RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/STREET3").InnerText, Encoding.UTF8);
            //string STREET3 = sr.ReadToEnd();
            //string CITY = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/CITY").InnerText;
            //string CITY_CODE = STREET3;
            //sr = new System.IO.StreamReader(RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/REGION").InnerText, Encoding.UTF8);
            //string REGION = sr.ReadToEnd();
            //string REGION_CODE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/REGION_CODE").InnerText;
            //string POST_CODE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/POST_CODE").InnerText;
            //string NAME_LAST = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/NAME_LAST").InnerText;
            string NAME_FIRST = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/NAME_FIRST").InnerText;
            //System.IO.StreamReader sr = new System.IO.StreamReader(NAME_FIRST, Encoding.UTF8);
            //NAME_FIRST = sr.ReadToEnd();
            string NAME_LAST = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/NAME_LAST").InnerText;
            string CONSUMER = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/CONSUMER").InnerText;
            string PHONE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/PHONE").InnerText;
            string STREET1 = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/STREET1").InnerText;
            string STREET2 = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/STREET2").InnerText;
            string STREET3 = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/STREET3").InnerText;
            string CITY = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/CITY").InnerText;
            string CITY_CODE = STREET3;
            string REGION = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/REGION").InnerText;
            string REGION_CODE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/REGION_CODE").InnerText;
            string POST_CODE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/CHANGE_GSPN_CUSTOMER_REQUEST/POST_CODE").InnerText;
            AddToLogFile(NAME_FIRST);
            AddToLogFile(NAME_LAST);
            AddToLogFile(STREET1);
            NAME_FIRST = System.Net.WebUtility.UrlEncode(NAME_FIRST);
            AddToLogFile(NAME_FIRST);
            NAME_LAST = System.Net.WebUtility.UrlEncode(NAME_LAST);
            AddToLogFile(NAME_LAST);
            STREET1 = System.Net.WebUtility.UrlEncode(STREET1);
            AddToLogFile(STREET1);
            STREET2 = System.Net.WebUtility.UrlEncode(STREET2);
            CITY = System.Net.WebUtility.UrlEncode(CITY);
            REGION = System.Net.WebUtility.UrlEncode(REGION);

            //cmd=SVCPopCustomerCreateCmd&numPerPage=100&currPage=0&BP_TYPE=C001&TITLE=0002&NAME_FIRST=%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9+%D0%AE%D1%80%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&NAME_LAST=%D0%94%D1%8E&CONSUMER=4025679954&UNIQUE_ID=&HOME_PHONE=9242221144&OFFICE_PHONE=9242221144&OFFICE_PHONE_EXT=&MOBILE_PHONE=9242221144&FAX=&EMAIL=&CONTACT_FLAG=3&STREET1=%D0%90%D1%80%D1%82%D0%B5%D0%BC%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB%2C+%D0%B4.55%2C+%D0%BA%D0%BE%D1%80%D0%BF.%D0%90%2C&STREET2=%D0%BA%D0%B2.10&STREET3=1007&DISTRICT=&CITY=%D0%A5%D0%B0%D0%B1%D0%B0%D1%80%D0%BE%D0%B2%D1%81%D0%BA&SEL_CITY_CODE=1007&CITY_CODE=1007&REGION=%D0%A5%D0%B0%D0%B1%D0%B0%D1%80%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9+%D0%BA%D1%80%D0%B0%D0%B9&REGION_CODE=27&COUNTRY=RU&POST_CODE=680001

            string par = "cmd=SVCPopCustomerCreateCmd&numPerPage=100&currPage=0&BP_TYPE=C001&TITLE=0002&NAME_FIRST=" + NAME_FIRST + "&NAME_LAST=" + NAME_LAST + "&CONSUMER=" + CONSUMER + "&UNIQUE_ID=&HOME_PHONE=" + PHONE + "&OFFICE_PHONE=" + PHONE + "&OFFICE_PHONE_EXT=&MOBILE_PHONE=" + PHONE + "&FAX=&EMAIL=&CONTACT_FLAG=2&STREET1=" + STREET1 + "&STREET2=" + STREET2 + "&STREET3=" + STREET3 + "&DISTRICT=&CITY=" + CITY + "&SEL_CITY_CODE=" + CITY_CODE + "&CITY_CODE=" + CITY_CODE + "&REGION=" + REGION + "&REGION_CODE=" + REGION_CODE + "&COUNTRY=RU&POST_CODE=" + POST_CODE;
            AddToLogFile(par);
            //cmd=GeneralPartDetailCmd&numPerPage=100&currPage=0&material=GH97-14630A&fileType=B&popupYn=Y&CorpCode=C941&partNo=GH97-14630A&partDesc=
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/common/SVCPopCustomerSearch.jsp";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ServicePoint.Expect100Continue = false;
            tQ.ContentLength = bytes.Length;
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;

            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//"click%20on%20%22MODEL%22%232"


    
            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();

                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("ChangeGSPNCustomer сработал catch " + responseFromServer);
                responseFromServer = "NO_ANSWER_Your session has expired";
                AddToLogFile("подменили responseFromServer на - " + responseFromServer);
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                //добавить поле message в котором написать No ewarranty registred
                OutXML.Save(OutXMLPath);
                return;
            }
         
            
           

            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode Response = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

            BODY.AppendChild(Response);

            OutXML.Save(OutXMLPath);
        }


        private void ADVOrderInfo(XmlDocument RecievedRequest, string OutXMLPath)
        {

            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            string MessageText = "";

            string SoldTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/customerCode").InnerText;
            string ShipTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/branchNo").InnerText;
            string fromDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ORDER_INFO_REQUEST/fromDate").InnerText;
            string toDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ORDER_INFO_REQUEST/toDate").InnerText;
            string confirmNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ORDER_INFO_REQUEST/confirmNo").InnerText;
            

            string alphabet = "1234567890";
            int i = 0;
            while (i < confirmNo.Length)
            {
                if (confirmNo.IndexOfAny(alphabet.ToCharArray(), i) == -1)
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    MessageText = "Check confirmNo";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                ++i;
            }

            if (confirmNo.Length != 10)
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Check confirmNo";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

          
                string responseFromServer = "";
                while (responseFromServer == "")
                {
                    if (!CheckGSPNAuthorization())
                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageTextError = "GSPN authorization fail";
                        AddMessageToHeader(OutXML, MessageTextError);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    responseFromServer = ADVOrderInfoRequest(confirmNo, SoldTo, ShipTo, fromDate, toDate);
            }

            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //вот этим превращаем в объект
            var rc = js.Deserialize<RootClassOrderInformation>(responseFromServer);

            if (rc.success)
            {
                WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                XmlNode BODY = OutXML.CreateElement("BODY");
                OutXML.DocumentElement.AppendChild(BODY);
                XmlNode ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

                foreach (soList part in rc.soList)
                {

                    XmlNode ResponsePART = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE_PART");

                    XmlNode node = OutXML.CreateElement("shipTo");
                    node.InnerText = part.shipTo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("soNo");
                    node.InnerText = part.soNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("poNo");
                    node.InnerText = part.poNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("poDate");
                    node.InnerText = part.poDate;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("soItemNo");
                    node.InnerText = part.soItemNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("poPart");
                    node.InnerText = part.poPart;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("poQty");
                    node.InnerText = part.poQty;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("soPart");
                    node.InnerText = part.soPart;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("doQty");
                    node.InnerText = part.doQty;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("billQty");
                    node.InnerText = part.billQty;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("price");
                    node.InnerText = part.price;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("amount");
                    node.InnerText = part.amount;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("status");
                    node.InnerText = part.status;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("remark");
                    node.InnerText = part.remark;
                    ResponsePART.AppendChild(node);

                    ResponseROOT.AppendChild(ResponsePART);

                }
                BODY.AppendChild(ResponseROOT);
                OutXML.Save(OutXMLPath);

            }
            else
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "There is no data for this confirmNo";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                
            }


        }
        
        private void POShippingInformation(XmlDocument RecievedRequest, string OutXMLPath)

        {

            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            string ShipTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/branchNo").InnerText;
            string fromDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/PO_SHIPPING_INFORMATION_REQUEST/fromDate").InnerText;
            string toDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/PO_SHIPPING_INFORMATION_REQUEST/toDate").InnerText;

            //DateTime DATEfromDate = Convert.ToDateTime(fromDate);
            //DateTime DATEtoDate = Convert.ToDateTime(toDate);

            DateTime DATEfromDate = DateTime.ParseExact(fromDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            DateTime DATEtoDate = DateTime.ParseExact(toDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

            if (DATEfromDate > DATEtoDate || DATEfromDate.AddDays(7) < DATEtoDate)
            {
           
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    //добавить поле message в котором написать неверная работа с датами. Уточните даты в запросе
                    OutXML.Save(OutXMLPath);
                    return;

            }

            fromDate = DATEfromDate.ToString("d");
            toDate = DATEtoDate.ToString("d");

            string responseFromServer = "";
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageTextError = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageTextError);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = POShippingInformationRequest(ShipTo, fromDate, toDate);
            }

            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //вот этим превращаем в объект
            var rc = js.Deserialize<RootClassShippingInfo>(responseFromServer);

            if (rc == null)
            {

                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                //добавить поле message в котором написать некорректный ответ от ГСПН
                OutXML.Save(OutXMLPath);
                return;
            }

            if (rc.error == false)
            {
                WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                XmlNode BODY = OutXML.CreateElement("BODY");
                OutXML.DocumentElement.AppendChild(BODY);

                string invoiceNo = "";
                DateTime DATEinvoiceDate;
                string invoiceDate = "";
                string invoiceLocalNo = "";
                string accountingDocNo = "";
                string doNo = "";
                DateTime DATEdoDate;
                string doDate = "";
                string doTrNo = "";
                string shipTo = "";
                Double amount = 0;
                Double tax = 0;
                Double totamount = 0;

                XmlNode ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

                XmlNode nodeinvoiceNo = OutXML.CreateElement("invoiceNo");
                XmlNode nodeinvoiceDate = OutXML.CreateElement("invoiceDate");
                XmlNode nodeinvoiceLocalNo = OutXML.CreateElement("invoiceLocalNo");               
                XmlNode nodeaccountingDocNo = OutXML.CreateElement("accountingDocNo");                
                XmlNode nodedoNo = OutXML.CreateElement("doNo");               
                XmlNode nodeidoDate = OutXML.CreateElement("doDate");              
                XmlNode nodedoTrNo = OutXML.CreateElement("doTrNo");              
                XmlNode nodeshipTo = OutXML.CreateElement("shipTo");            
                XmlNode nodeamount = OutXML.CreateElement("amount");             
                XmlNode nodetax = OutXML.CreateElement("tax");           
                XmlNode nodetotamount = OutXML.CreateElement("totamount");



                //XmlNode nodeinvoiceNo, nodeinvoiceDate, nodeinvoiceLocalNo, nodeaccountingDocNo, nodedoNo, nodeidoDate, nodedoTrNo, nodeshipTo, nodeamount, nodetax, nodetotamount;

                List<PtDataList> sorted = rc.PtDataList.OrderBy(x => x.deliveryNo.ToLower()).ToList();//.ThenBy(p=>p..ToList();

                var delNumbers = (from s in sorted
                                  select new { delNo = s.deliveryNo })
                                  .Distinct()
                                  .ToList();

                

                foreach (PtDataList part in sorted)//rc.PtDataList)
                {
                    //List<PtDataList> voteti = sorted.Where(x => x.deliveryNo == part.deliveryNo).ToList();

                    if (doNo == "")//первый инвойс

                    {
                        invoiceNo = part.invoiceNo;
                        DATEinvoiceDate = Convert.ToDateTime(part.invoiceDate);
                        invoiceDate = DATEinvoiceDate.ToString("yyyyMMdd");
                        invoiceLocalNo = part.invoiceNo;
                        accountingDocNo = "";
                        doNo = part.deliveryNo;
                        DATEdoDate = Convert.ToDateTime(part.giDate);
                        doDate = DATEdoDate.ToString("yyyyMMdd"); ;
                        foreach (trkEvet tracking in part.trkEvet)
                        {
                            if (tracking.trackingNo != "")
                            { doTrNo = tracking.trackingNo; break; }
                            else
                            {
                                doTrNo = tracking.doNo; break;
                            }
                        }
                        shipTo = ShipTo;

                        ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");
                        BODY.AppendChild(ResponseROOT);

                    }

                    if (doNo != part.deliveryNo)//смена инвойса

                    {

                        //завершаем старый
                        nodetotamount.InnerText = amount.ToString().Replace(",", ".");
                        ResponseROOT.InsertBefore(nodetotamount, ResponseROOT.FirstChild);

                        nodetax.InnerText = tax.ToString().Replace(",", ".");
                        ResponseROOT.InsertBefore(nodetax, ResponseROOT.FirstChild);

                        nodeamount.InnerText = (amount - tax).ToString().Replace(",", ".");
                        ResponseROOT.InsertBefore(nodeamount, ResponseROOT.FirstChild);

                        nodeshipTo.InnerText = shipTo;
                        ResponseROOT.InsertBefore(nodeshipTo, ResponseROOT.FirstChild);

                        nodedoTrNo.InnerText = doTrNo;
                        ResponseROOT.InsertBefore(nodedoTrNo, ResponseROOT.FirstChild);

                        nodeidoDate.InnerText = doDate;
                        ResponseROOT.InsertBefore(nodeidoDate, ResponseROOT.FirstChild);

                        nodedoNo.InnerText = doNo;
                        ResponseROOT.InsertBefore(nodedoNo, ResponseROOT.FirstChild);

                        nodeaccountingDocNo.InnerText = accountingDocNo;
                        ResponseROOT.InsertBefore(nodeaccountingDocNo, ResponseROOT.FirstChild);

                        nodeinvoiceLocalNo.InnerText = invoiceLocalNo;
                        ResponseROOT.InsertBefore(nodeinvoiceLocalNo, ResponseROOT.FirstChild);

                        nodeinvoiceDate.InnerText = invoiceDate;
                        ResponseROOT.InsertBefore(nodeinvoiceDate, ResponseROOT.FirstChild);

                        nodeinvoiceNo.InnerText = invoiceNo;
                        ResponseROOT.InsertBefore(nodeinvoiceNo, ResponseROOT.FirstChild);

                        BODY.AppendChild(ResponseROOT);

                        //создаем новый

                        //рассчитаем параметры
                        invoiceNo = part.invoiceNo;
                        DATEinvoiceDate = Convert.ToDateTime(part.invoiceDate);
                        invoiceDate = DATEinvoiceDate.ToString("yyyyMMdd");
                        invoiceLocalNo = part.invoiceNo;
                        accountingDocNo = "";
                        doNo = part.deliveryNo;
                        DATEdoDate = Convert.ToDateTime(part.giDate);
                        doDate = DATEdoDate.ToString("yyyyMMdd"); ;
                        foreach (trkEvet tracking in part.trkEvet)
                        {
                            if (tracking.trackingNo != "")
                            { doTrNo = tracking.trackingNo; break; }
                            else
                            {
                                doTrNo = tracking.doNo; break;
                            }
                        }
                        shipTo = ShipTo;
                        //создадим поля
                        ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");
                        BODY.AppendChild(ResponseROOT);

                        nodeinvoiceNo = OutXML.CreateElement("invoiceNo");
                        nodeinvoiceDate = OutXML.CreateElement("invoiceDate");
                        nodeinvoiceLocalNo = OutXML.CreateElement("invoiceLocalNo");
                        nodeaccountingDocNo = OutXML.CreateElement("accountingDocNo");
                        nodedoNo = OutXML.CreateElement("doNo");
                        nodeidoDate = OutXML.CreateElement("doDate");
                        nodedoTrNo = OutXML.CreateElement("doTrNo");
                        nodeshipTo = OutXML.CreateElement("shipTo");
                        nodeamount = OutXML.CreateElement("amount");
                        nodetax = OutXML.CreateElement("tax");
                        nodetotamount = OutXML.CreateElement("totamount");
                        amount = 0;
                        tax = 0;
                        totamount = 0;

                    }

                    XmlNode ResponsePART = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE_ITEM");
                    
                    //XmlNode node = OutXML.CreateElement("shipTo");
                    //node.InnerText = part.shipTo;
                    //ResponsePART.AppendChild(node);

                    XmlNode node = OutXML.CreateElement("invoiceNo");
                    node.InnerText = part.invoiceNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("invoiceItemNo");
                    node.InnerText = part.invoiceItemNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("deliveryNo");
                    node.InnerText = part.deliveryNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("doItemNo");
                    node.InnerText = part.deliveryItemNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("poNo");
                    node.InnerText = part.poNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("poPartNo");
                    node.InnerText = part.orderedPart;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("soNo");
                    node.InnerText = part.soNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("soItemNo");
                    node.InnerText = part.soItemNo;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("soPartNo");
                    node.InnerText = part.shippedPart;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("qty");
                    node.InnerText = part.doQty;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("price");
                    node.InnerText = part.netValue;
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("amount");
                    node.InnerText = part.amount.Replace(",", "");
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("description");
                    node.InnerText = "";
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("coreFlag");
                    node.InnerText = "";
                    ResponsePART.AppendChild(node);

                    node = OutXML.CreateElement("status");
                    node.InnerText = part.status;
                    ResponsePART.AppendChild(node);

                    ResponseROOT.AppendChild(ResponsePART);

                    tax = tax + Convert.ToDouble(part.tax.Replace(",","").Replace(".",","));
                    amount = amount + Convert.ToDouble(part.amount.Replace(",", "").Replace(".", ","));
                    
                }
                //завершим последний
                             
                nodetotamount.InnerText = amount.ToString().Replace(",", ".");
                ResponseROOT.InsertBefore(nodetotamount, ResponseROOT.FirstChild);

                nodetax.InnerText = tax.ToString().Replace(",", ".");
                ResponseROOT.InsertBefore(nodetax, ResponseROOT.FirstChild);

                nodeamount.InnerText = (amount - tax).ToString().Replace(",", ".");
                ResponseROOT.InsertBefore(nodeamount, ResponseROOT.FirstChild);

                nodeshipTo.InnerText = shipTo;
                ResponseROOT.InsertBefore(nodeshipTo, ResponseROOT.FirstChild);

                nodedoTrNo.InnerText = doTrNo;
                ResponseROOT.InsertBefore(nodedoTrNo, ResponseROOT.FirstChild);

                nodeidoDate.InnerText = doDate;
                ResponseROOT.InsertBefore(nodeidoDate, ResponseROOT.FirstChild);

                nodedoNo.InnerText = doNo;
                ResponseROOT.InsertBefore(nodedoNo, ResponseROOT.FirstChild);

                nodeaccountingDocNo.InnerText = accountingDocNo;
                ResponseROOT.InsertBefore(nodeaccountingDocNo, ResponseROOT.FirstChild);

                nodeinvoiceLocalNo.InnerText = invoiceLocalNo;
                ResponseROOT.InsertBefore(nodeinvoiceLocalNo, ResponseROOT.FirstChild);

                nodeinvoiceDate.InnerText = invoiceDate;
                ResponseROOT.InsertBefore(nodeinvoiceDate, ResponseROOT.FirstChild);

                nodeinvoiceNo.InnerText = invoiceNo;
                ResponseROOT.InsertBefore(nodeinvoiceNo, ResponseROOT.FirstChild);

                BODY.AppendChild(ResponseROOT);

                OutXML.Save(OutXMLPath);

            }
            else
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                //добавить поле message в котором написать не получена информация о номере подвтерждения
                OutXML.Save(OutXMLPath);

            }


        }

        private void ADVMGDinfo(XmlDocument RecievedRequest, string OutXMLPath)
        {


            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            string MessageText = "";

            //string SoldTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/customerCode").InnerText;
            string ShipTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/branchNo").InnerText;
            string fromDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_MGD_INFO_REQUEST/fromDate").InnerText;
            string toDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_MGD_INFO_REQUEST/toDate").InnerText;
            string serialNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_MGD_INFO_REQUEST/serialNo").InnerText;

          
            if (serialNo == "")
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Check serial no";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            DateTime DATEfromDate = DateTime.ParseExact(fromDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            DateTime DATEtoDate = DateTime.ParseExact(toDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

            if (DATEfromDate > DATEtoDate || DATEfromDate.AddDays(7) < DATEtoDate)
            {

                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Check dates";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;

            }

            string fromDateDots = DATEfromDate.ToString("d");
            string toDateDots = DATEtoDate.ToString("d");

            string responseFromServer = "";
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageTextError = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageTextError);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = ADVMGDinfoRequest(ShipTo, fromDate, toDate, fromDateDots, toDateDots, serialNo);
            }

            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //вот этим превращаем в объект
            var rc = js.Deserialize<RootClassMGDStatus>(responseFromServer);

            if (rc == null)
            {

                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Desiarilization error when try to get MGD list";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            if (rc.success && !rc.error)
            {

                List<ET_DIA_HISTORY> sorted = rc.ET_DIA_HISTORY.OrderByDescending(x => x.SEQ.ToLower()).ToList();//по убыванию

                foreach (ET_DIA_HISTORY MGD in sorted)
                {

                    if (MGD.CHECK_RESULT == "PASS")
                    {
                        WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        XmlNode BODY = OutXML.CreateElement("BODY");
                        OutXML.DocumentElement.AppendChild(BODY);
                        XmlNode ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

                        XmlNode ResponseITEM = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_ITEM");

                        XmlNode ANSWnode = OutXML.CreateElement("result");
                        ANSWnode.InnerText = "PASS";
                        ResponseITEM.AppendChild(ANSWnode);

                        ANSWnode = OutXML.CreateElement("TEMP_OBJECT_ID");
                        ANSWnode.InnerText = MGD.TEMP_OBJECT_ID;
                        ResponseITEM.AppendChild(ANSWnode);

                        ANSWnode = OutXML.CreateElement("SEQ");
                        ANSWnode.InnerText = MGD.SEQ;
                        ResponseITEM.AppendChild(ANSWnode);

                        ANSWnode = OutXML.CreateElement("trNo");
                        ANSWnode.InnerText = MGD.OBJECT_ID;
                        ResponseITEM.AppendChild(ANSWnode);

                        ResponseROOT.AppendChild(ResponseITEM);

                        BODY.AppendChild(ResponseROOT);
                        OutXML.Save(OutXMLPath);
                        return;

                    }
                }
            }

            WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            MessageText = "There is no Passed MGD";
            AddMessageToHeader(OutXML, MessageText);
            OutXML.Save(OutXMLPath);
            
        }

        private void ADVMGDDetail(XmlDocument RecievedRequest, string OutXMLPath)
        {


            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            //string SoldTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/customerCode").InnerText;
            string ShipTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/branchNo").InnerText;
            string IV_OBJECT_ID = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_MGD_DETAIL_REQUEST/IV_OBJECT_ID").InnerText;
            string IV_TEMP_OBJECT_ID = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_MGD_DETAIL_REQUEST/IV_TEMP_OBJECT_ID").InnerText;
            string SEQ = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_MGD_DETAIL_REQUEST/SEQ").InnerText;


            if (SEQ == ""|| IV_TEMP_OBJECT_ID == "")
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                string MessageText = "SEQ and IV_TEMP_OBJECT_ID should be filled";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            string responseFromServer = "";
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageTextError = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageTextError);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = ADVMGDDetailRequest(ShipTo, IV_OBJECT_ID, IV_TEMP_OBJECT_ID, SEQ);
            }

            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //вот этим превращаем в объект
            var rc = js.Deserialize<RootClassMGDDetail>(responseFromServer);

            if (rc == null)
            {

                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                string MessageText = "Desirialization fail. Wrong answer from GSPN";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            if (rc.success && !rc.error)
            {

                //List<ET_DIA_HISTORY> sorted = rc.ET_DIA_HISTORY.OrderBy(x => x.SEQ.ToLower()).ToList();
                WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                XmlNode BODY = OutXML.CreateElement("BODY");
                OutXML.DocumentElement.AppendChild(BODY);
                XmlNode ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

                foreach (ET_DIA_DETAIL test in rc.ET_DIA_DETAIL)
                {

                    XmlNode ResponseITEM = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_ITEM");

                    XmlNode ANSWnode = OutXML.CreateElement("name");
                    ANSWnode.InnerText = test.GD_GROUP_DESC + "-" + test.GD_SUB_DESC;
                    ResponseITEM.AppendChild(ANSWnode);

                    ANSWnode = OutXML.CreateElement("result");
                    ANSWnode.InnerText = test.CHECK_RESULT;
                    ResponseITEM.AppendChild(ANSWnode);

                    ResponseROOT.AppendChild(ResponseITEM);

                }

                BODY.AppendChild(ResponseROOT);
                OutXML.Save(OutXMLPath);
            }
            else
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                string MessageText = "There is no test list for this parameters";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

        }

        private void ADVSTDELPART(XmlDocument RecievedRequest, string OutXMLPath, string XMLData)

        {

            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            string SoldTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/customerCode").InnerText;
            string ShipTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/branchNo").InnerText;
            string IV_PARTS_CODE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_INFO/IV_PARTS_CODE").InnerText;
            string seqNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_INFO/seqNo").InnerText;
            string IV_DATE = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_INFO/IV_DATE").InnerText;
            string IV_TIME = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_INFO/IV_TIME").InnerText;
            string jobServiceType = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/jobServiceType").InnerText;
            string objectID = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/objectID").InnerText;
            string gi_ASC_JOB_NO = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/gi_ASC_JOB_NO").InnerText;
            string customerCode = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/customerCode").InnerText;
            string MODEL = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/MODEL").InnerText;
            string SERIAL = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/SERIAL").InnerText;
            string IMEI = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/IMEI").InnerText;
            string svcPrcd = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_ST_DELPART_GLOBAL/svcPrcd").InnerText;

            string par = "openTabID=&jobServiceType="+ jobServiceType + "&SOLASTCHANGEDDATE=" + IV_DATE + "&SOLASTCHANGEDTIME=" + IV_TIME + "&STATE2=&LAST_APP_DATE=&frYear=&toYear=&frMonth=&toMonth=&IV_PARTS_CODE=" + IV_PARTS_CODE + "&IV_DATE=" + IV_DATE + "&fromListButton=&SAWPART=" + IV_PARTS_CODE +
                "&PART_SERIAL=&PART_TERM=I&soDetailType=&jspName=&dataChange=X&p_listCall=X&cmd=ServiceOrderPartsDeleteCmd&objectID=" + objectID + "&gi_ASC_JOB_NO=" + gi_ASC_JOB_NO + "&assignedFlag=X&ascCode=" + ShipTo + "&customerCode=" + customerCode + "&msg_seqno=&msgText=&isawNo=&partsUsed=&wtyInOut=LP&IV_OBJKEY=&file_" +
                "name=&fileSize=&Ctype=IRIS-C%2CIRIS-D%2CIRIS-R%2CIRIS-S&Code=&MODEL=" + MODEL + "&SERIAL=" + SERIAL + "&IMEI=" + IMEI + "&PRODUCT_DATE=&SYMPTOM_CAT1=&SYMPTOM_CAT2=&SYMPTOM_CAT3=&claimno=&wty_err_flag=&MBLNR=&MJAHR=&gi_material=&gi_qty=&gi_seq_no=&gi_engineer=&" +
                "gi_engineer_nm=&gi_postingFlag=&gi_partWty=&cancelFlag=&svcPrcd=" + svcPrcd + "&quotationFlag=&billingSearch=&hasWtyBilling=&model_p=&serialNo_p=&ASC_CODE_p=&IV_OBJECT_ID=" + objectID + "&interMessageType=&IRIS_CONDI=&IRIS_SYMPT=&IRIS_DEFECT=&IRIS_REPAIR=&IRIS_CONDI_DESC=" +
                "&IRIS_SYMPT_DESC=&IRIS_DEFECT_DESC=&IRIS_REPAIR_DESC=&RetailInstallation=&additionalGasChargeForDVM=&canRedoMinorOption=&sameSAWCatCode=&canExtraPersonOption=&canExtraMileageHAOption=&sawExistCompressorSerialApproved=&" +
                "sawExistSerialNoValidationApproved=&defectType=&svcProd=&sawExistLabor=false&AUTH_GR=HHP01&PURCHASE_PLACE=&SAW_CATEGORY=&REASON=&currStatus=ST030&autoDo=&cicProd=HHP&ewpYn=";

            string responseFromServer = "";
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageText = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = ADV_ST_DELPART_REQUEST(seqNo, par);
            }

            if (responseFromServer.IndexOf("success\":true") > -1)

            {
                WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                OutXML.Save(OutXMLPath);
                return;
            }
            else
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                OutXML.Save(OutXMLPath);
                return;
            }

        }

        private string ADV_ST_DELPART_REQUEST(string seqNo, string par)
        {
            
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do?PARTS_SEQ_NO="+seqNo;
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.Referer = "https://biz5.samsungcsportal.com/gspn/operate.do?UI=&currTabId=divJob#tabInfoHref";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            //tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ServicePoint.Expect100Continue = false;
            tQ.ContentLength = bytes.Length;
            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//"click%20on%20%22MODEL%22%232"

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("ADV_ST_DELPART_REQUEST сработал catch " + responseFromServer);
            }
            return responseFromServer;
        }

        private void ServicePartReturn(XmlDocument RecievedRequest, string OutXMLPath, string XMLData)

        {

            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);

            string SoldTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/customerCode").InnerText;
            string ShipTo = RecievedRequest.SelectSingleNode("MESSAGE/HEADER/branchNo").InnerText;
            string doDate = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_SPR_REQUEST/doDate").InnerText; ;
            string doNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_SPR_REQUEST/doNo").InnerText;
            string comment = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_SPR_REQUEST/comment").InnerText;

            if (comment == "" || doNo == "" || doDate == "" )
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                string MessageText = "check input parameters";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            DateTime DATEdoDate = DateTime.ParseExact(doDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

            string DateWithDots = DATEdoDate.ToString("d");


            string responseFromServer = "";
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageText = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = SPR_PrReturnListItemCmd(SoldTo, DateWithDots, doNo);
            }

            var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //вот этим превращаем в объект
            var rc = js.Deserialize<RootPrReturnListItemCmd>(responseFromServer);

            if (rc == null)
            {

                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                string MessageText = "Desiarilization error when try to get billing list";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            if (rc.success && !rc.error)
            {

                //формат запроса на создание претензии
                //cmd = CreditRequestVerifyCmd & action_type = PR_CREATE_VERIFY & это меняется в зависимости от верифай или сэйв

                //intvl = &i_act = &i_type = &i_ar_doc_id = &i_delivery_no = 8412942479 & i_delivery_date = 04.02.2018 & i_po_no = &i_po_date = &i_gi_date = 04.02.2018 & i_billing_date = 04.02.2018 & i_serial_no = &i_billing_no_h = &PI_BUKRS = C941 & PI_TESTRUN = &do_type = &IV_COMPANY = C941 & i_remarks1 = &do_item_no = &stock_type = &gr_qty = 0 & refSite = &Partner = 0009341483 & i_return_req_no = &i_reason_code = 821 & i_asc_ref_no = &i_title = 8412942 & i_description = 12 & i_model_code = &i_tracking_no = &i_total_amount = 4825.70 &

                //            i_imei = &    !!!!!!!!!!!!У этой середины новый формат. ниже описан. Верх и низ прежние
                //            i_iris = &
                //            i_claim_qty = 1 &
                //            i_returnable_qty = &
                //            i_billing_no = 3288067951 &
                //            i_billing_item_no = 000960 &
                //            i_material_no = GH82 - 12267A &
                //            i_sales_unit = PC &
                //            i_current_price = 4825.70 &
                //            i_current_amount = 4825.70 &
                //            i_new_price = 0.00 &
                //            i_tmp_tax = 0.00 &
                //            i_new_amount = 4825.70 &
                //            i_currency = RUB &
                //            i_plant = V902 &
                //            i_storage_location = RS10 &
                //            i_item_short_text = SVC + PBA - PBA + MAIN(COMM) % 2CSM - T585 % 2C % 3BSM - T585 % 2C &
                //            i_pricing_date = 20180202 &
                //            COMPANY = C941 &
                //            ASC_ACCTNO = 0009341483 &
                //            ASC_CODE = 0009341483 &
                //            FLAG = D &


                //         i_order_type = &i_sales_org = 9414 & i_distr_channel = 10 & i_division = Z1 & i_order_reason = &i_soldto_party = 0009341483 & i_billto_party = 0009341483 & i_payer = 0009341483 & i_shipto_party = 0009341483 & i_postal_code = 680000 & i_city =% D0 % A5 % D0 % B0 % D0 % B1 % D0 % B0 % D1 % 80 % D0 % BE % D0 % B2 % D1 % 81 % D0 % BA & i_street =% D1 % 83 % D0 % BB.+% D0 % 9B % D0 % B5 % D0 % BD % D0 % B8 % D0 % BD % D0 % B3 % D1 % 80 % D0 % B0 % D0 % B4 % D1 % 81 % D0 % BA % D0 % B0 % D1 % 8F % 2C +% D0 % B4.+ 63 & i_country_code = RU & i_item_summary = 424849.42 & i_h_currency = RUB & i_reference_billing_no = 3288067951 & i_inco_terms_1 = CPT & i_inco_terms_2 = Normal + Delivery & i_payment_terms_2 = 60 + Days + from + Doc.+ Date & i_payment_terms_1 = P060 & st_f_date = &st_t_date = &st_credit_req_no = &st_asc_ref_no = &st_bill_no = &st_reason = &st_status =

                Decimal amount = 0;
                string ParHeader = "";
                string ParParts = "";
                string ParBottom = "";
                string FinalPar = "";

                string currentPartNo = "";
                string currentqty = "";
                string invoiceNo = "";
                XmlNode xnode = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_SPR_REQUEST");
                foreach (XmlNode item in xnode)
                {
                    if (item.Name == "ADV_SPR_REQUEST_ITEM")
                    {
                        foreach (XmlNode part in item.ChildNodes)
                        {
                            if (part.Name == "partNo")
                            {
                                currentPartNo = part.InnerText;
                            }
                            if (part.Name == "qty")
                            {
                                currentqty = part.InnerText;
                            }
                            if (part.Name == "invoiceNo")
                            {
                                invoiceNo = part.InnerText;
                            }
                            if (part.Name == "invoiceItemNo")
                            {

                                string invoiceItemNoReq = part.InnerText;
                                var PartInfoFromGSPN = rc.ptItem.Where(s => s.posnr == invoiceItemNoReq);

                                foreach (ptItem w in PartInfoFromGSPN)
                                {
                                    if (currentPartNo != w.matnr)
                                    {
                                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                                        string MessageText = "Something wrong. Data in GSPN not mached with sended data. Part:" + currentPartNo;
                                        AddMessageToHeader(OutXML, MessageText);
                                        OutXML.Save(OutXMLPath);
                                        return;
                                    }


                                    Decimal currentAmount = Decimal.Parse(w.netpr.Replace(".", ",")) * Decimal.Parse(currentqty);//
                                    amount = amount + currentAmount;
                                    //ParParts = ParParts + "i_imei=&i_iris=&i_claim_qty=" + currentqty + "&i_returnable_qty=&i_billing_no=" + invoiceNo + "&i_billing_item_no=" + invoiceItemNoReq + "&i_material_no=" + currentPartNo + "&i_sales_unit=" + w.vrkme + "&i_current_price=" + w.netpr + "&i_current_amount=" + currentAmount.ToString().Replace(",", ".") + "&i_new_price=0.00&i_tmp_tax=0.00&i_new_amount=" + currentAmount.ToString().Replace(",", ".") + "&i_currency=" + w.waers + "&i_plant=" + w.werks + "&i_storage_location=" + w.lgort + "&i_item_short_text=" + System.Net.WebUtility.UrlEncode(w.arktx).Replace("(", "%28").Replace(")", "%29") + "&i_pricing_date=" + w.prsdt + "&COMPANY=C941&ASC_ACCTNO=" + SoldTo + "&ASC_CODE=" + SoldTo + "&FLAG=D&";
                                    
                                    
                                    //новый формат середины. выше старый = закоменчен
                                   
                                    //i_soNo = &
                                    //i_eng = &
                                    //symptom_level1 = &
                                    //symptom_level2 = &
                                    //symptom_level3 = &
                                    //i_imei = &
                                    //i_claim_qty = 1 &
                                    //i_returnable_qty = &
                                    //i_billing_no = 3297953942 &
                                    //i_billing_item_no = 000590 &
                                    //i_material_no = GH43 - 03241A &
                                    //i_sales_unit = PC &
                                    //i_current_price = 303.32 &
                                    //i_current_amount = 303.32 &
                                    //i_new_price = 0.00 &
                                    //i_tmp_tax = 0.00 &
                                    //i_new_amount = 303.32 &
                                    //i_currency = RUB &
                                    // i_plant = V902 &
                                    //i_storage_location = RS10 &
                                    // i_item_short_text = INNER + BATTERY + PACK - AB463446BU % 2C800MAH % 3BAB4 &
                                    // i_pricing_date = 20180518 &
                                    //COMPANY = C941 &
                                    //ASC_ACCTNO = 0009341483 &
                                    //ASC_CODE = 0009341483 &
                                    //inventoryAscCode = 0009341483 &
                                    //FLAG = D &
                                    //i_iris = &
                                    ParParts = ParParts + "i_soNo=&i_eng=&symptom_level1=&symptom_level2=&symptom_level3=&i_imei=&i_claim_qty=" + currentqty + "&i_returnable_qty=&i_billing_no=" + invoiceNo + "&i_billing_item_no=" + invoiceItemNoReq + "&i_material_no=" + currentPartNo + "&i_sales_unit=" + w.vrkme + "&i_current_price=" + w.netpr + "&i_current_amount=" + currentAmount.ToString().Replace(",", ".") + "&i_new_price=0.00&i_tmp_tax=0.00&i_new_amount=" + currentAmount.ToString().Replace(",", ".") + "&i_currency=" + w.waers + "&i_plant=" + w.werks + "&i_storage_location=" + w.lgort + "&i_item_short_text=" + System.Net.WebUtility.UrlEncode(w.arktx).Replace("(", "%28").Replace(")", "%29") + "&i_pricing_date=" + w.prsdt + "&COMPANY=C941&ASC_ACCTNO=" + SoldTo + "&ASC_CODE=" + SoldTo + "&inventoryAscCode=" + SoldTo + "&FLAG=D&i_iris=&";
                                    break;
                                }
                            }

                        }
                    }


                }

                string City = System.Net.WebUtility.UrlEncode(rc.ptHeader[0].ort01).Replace("(", "%28").Replace(")", "%29"); ;
                string Street = System.Net.WebUtility.UrlEncode(rc.ptHeader[0].stras).Replace("(", "%28").Replace(")", "%29"); ;
                ParBottom = "i_order_type=&i_sales_org=" + rc.ptHeader[0].vkorg + "&i_distr_channel=" + rc.ptHeader[0].vtweg + "&i_division=" + rc.ptHeader[0].spart + "&i_order_reason=&i_soldto_party=" + SoldTo + "&i_billto_party=" + SoldTo + "&i_payer=" + SoldTo + "&i_shipto_party=" + ShipTo + "&i_postal_code=" + rc.ptHeader[0].pstlz + "&i_city=" + City + "&i_street=" + Street + "&i_country_code=" + rc.ptHeader[0].land1 + "&i_item_summary=" + rc.ptHeader[0].netwr + "&i_h_currency=" + rc.ptHeader[0].waers + "&i_reference_billing_no=" + invoiceNo + "&i_inco_terms_1=" + rc.ptHeader[0].inco1 + "&i_inco_terms_2=" + System.Net.WebUtility.UrlEncode(rc.ptHeader[0].inco2).Replace("(", "%28").Replace(")", "%29") + "&i_payment_terms_2=" + System.Net.WebUtility.UrlEncode(rc.ptHeader[0].text1).Replace("(", "%28").Replace(")", "%29") + "&i_payment_terms_1=" + rc.ptHeader[0].zterm + "&st_f_date=&st_t_date=&st_credit_req_no=&st_asc_ref_no=&st_bill_no=&st_reason=&st_status=";
                ParHeader = "intvl=&i_act=&i_type=&i_ar_doc_id=&i_delivery_no=" + doNo + "&i_delivery_date=" + DateWithDots + "&i_po_no=&i_po_date=&i_gi_date=" + DateWithDots + "&i_billing_date=" + DateWithDots + "&i_serial_no=&i_billing_no_h=&PI_BUKRS=C941&PI_TESTRUN=&do_type=&IV_COMPANY=C941&i_remarks1=&do_item_no=&stock_type=&gr_qty=0&refSite=&Partner=" + SoldTo + "&i_return_req_no=&i_reason_code=821&i_asc_ref_no=&i_title=" + doNo + "&i_description=" + System.Net.WebUtility.UrlEncode(comment).Replace("(", "%28").Replace(")", "%29") + "&i_model_code=&i_tracking_no=&i_total_amount=" + amount.ToString().Replace(",", ".") + "&";

                FinalPar = ParHeader + ParParts + ParBottom;

                responseFromServer = SPR_CreditRequestVerifyCmd(FinalPar);

                var js1 = new System.Web.Script.Serialization.JavaScriptSerializer();
                //вот этим превращаем в объект
                var rc1 = js.Deserialize<RootSPR_CreditRequestVerifyCmd>(responseFromServer);

                if (rc1 == null)
                {

                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageText = "Verify returned outformat answer. Derialization error";
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;
                }

                if (rc1.success && !rc1.error && rc1.retmsg == "Success")
                {


                    responseFromServer = SRP_CreditRequestSaveCmd(FinalPar);
                    if (responseFromServer != "")
                    {

                        int CreditRequestNoPosition = responseFromServer.IndexOf("Request No : ");
                        string CreditRequest = responseFromServer.Substring(CreditRequestNoPosition + 13, 10);
                        if (CreditRequestNoPosition == -1)
                        {
                            CreditRequestNoPosition = responseFromServer.IndexOf("Objectkey.value =");
                            CreditRequest = responseFromServer.Substring(CreditRequestNoPosition + 19, 10);
                        }
                        

                        WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        XmlNode BODY = OutXML.CreateElement("BODY");
                        OutXML.DocumentElement.AppendChild(BODY);
                        XmlNode ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

                        XmlNode ResponseITEM = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE_ITEM");

                        XmlNode ANSWnode = OutXML.CreateElement("creditRequestNo");
                        ANSWnode.InnerText = CreditRequest;
                        ResponseITEM.AppendChild(ANSWnode);

                        ResponseROOT.AppendChild(ResponseITEM);

                        BODY.AppendChild(ResponseROOT);
                        OutXML.Save(OutXMLPath);
                        return;
                    }
                    else

                    {
                        WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                        string MessageText = "Verify ok, Save - return empty answer";
                        AddMessageToHeader(OutXML, MessageText);
                        OutXML.Save(OutXMLPath);
                        return;
                    }

                }
                else
                {

                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageText = "Verify result:" + rc1.retmsg;
                    AddMessageToHeader(OutXML, MessageText);
                    OutXML.Save(OutXMLPath);
                    return;

                }


            }
        }
        //получает параметры аппаата которые установлены на заводе
        private void ADV_IMEIFabricInfo(XmlDocument RecievedRequest, string OutXMLPath)
        {


            CreateXMLFileForAnswer(OutXMLPath);
            XmlDocument OutXML = new XmlDocument();
            OutXML.Load(OutXMLPath);
            string MessageText = "";

            string serialNo = RecievedRequest.SelectSingleNode("MESSAGE/BODY/ADV_IMEI_FABRIC_INFO_REQUEST/serialNo").InnerText;


            if (serialNo == "")
            {
                WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                MessageText = "Check serial no";
                AddMessageToHeader(OutXML, MessageText);
                OutXML.Save(OutXMLPath);
                return;
            }

            string responseFromServer = "";
            while (responseFromServer == "")
            {
                if (!CheckGSPNAuthorization())
                {
                    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
                    string MessageTextError = "GSPN authorization fail";
                    AddMessageToHeader(OutXML, MessageTextError);
                    OutXML.Save(OutXMLPath);
                    return;
                }
                responseFromServer = ADV_IMEIFabricInfoRequest(serialNo);
            }

            //var js = new System.Web.Script.Serialization.JavaScriptSerializer();
            ////вот этим превращаем в объект
            //var rc = js.Deserialize<RootClassIMEIFabricInfo>(responseFromServer);

            //if (rc == null)
            //{

            //    WriteHeadOutXML(OutXML, "fail", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            //    MessageText = "Desiarilization error when try to get Fabric info";
            //    AddMessageToHeader(OutXML, MessageText);
            //    OutXML.Save(OutXMLPath);
            //    return;
            //}

            
            WriteHeadOutXML(OutXML, "ok", RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageKey").InnerText, RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText);
            XmlNode BODY = OutXML.CreateElement("BODY");
            OutXML.DocumentElement.AppendChild(BODY);
            XmlNode ResponseROOT = OutXML.CreateElement(RecievedRequest.SelectSingleNode("MESSAGE/HEADER/messageType").InnerText + "_RESPONSE");

            XmlNode ANSWnode = OutXML.CreateElement("result");
            ANSWnode.InnerText = responseFromServer;
            ResponseROOT.AppendChild(ANSWnode);

            BODY.AppendChild(ResponseROOT);
            OutXML.Save(OutXMLPath);
   
        }

        //ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ И ПРОЦЕДУРЫ К МЕТОДАМ
        //Ewarranty
        private string EwarrantyRequest(string SerialNo)
        {
            string responseFromServer = "";
            //String url = "https://ssl.samsung.ru/localCMS/warranty/GetWarranty?callback=jQuery18301642755231256492_1489321056465&&snimei=" + SerialNo;
            string url = "https://ssl.samsung.ru/localCMS/warranty/GetWarranty_BySN";
            string par = "SnImei=" + SerialNo;
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.ContentLength = bytes.Length;
            tQ.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
            tQ.Accept = "*/*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Referer = "http://www.samsung.com/ru/support/ewarranty/";
            tQ.Headers.Add("Origin", "https://www.samsung.com");
            tQ.KeepAlive = true;
            try
            {
                //HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                ////CookieCollection CookieC = tS.Cookies;
                ////string tC = tS.Headers["Set-Cookie"];
                ////Console.WriteLine(tS.Headers);
                //System.IO.Stream ReceiveStream = tS.GetResponseStream();
                //System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                //// string tC = tS.Headers["Set-Cookie"];
                //responseFromServer = sr.ReadToEnd();

                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();

                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }
            catch { responseFromServer = ""; }
            return responseFromServer;
        }


//ADVPartsInfo
        //стандартный запрос, возвращает результат запроса
        private string ADVPartsInfoRequest(string partNo, bool StandByCookies)
        {

            string par = "cmd=GeneralPartDetailCmd&numPerPage=100&currPage=0&material=" + partNo + "&fileType=B&popupYn=Y&CorpCode=C941&partNo=" + partNo + "&partDesc=";
            //cmd=GeneralPartDetailCmd&numPerPage=100&currPage=0&material=GH97-14630A&fileType=B&popupYn=Y&CorpCode=C941&partNo=GH97-14630A&partDesc=
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/master/part/GeneralPartInfo.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ServicePoint.Expect100Continue = false;
            tQ.ContentLength = bytes.Length;
            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;

            tQ.Headers.Add("Cache-Control", "no-cache");
            //tQ.CookieContainer = new CookieContainer();
            //tQ.CookieContainer = GSPNCoockies;
            //tQ.CookieContainer = form.Authorization();
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            //if (StandByCookies)
            //    {
            //        ckCol.Add(GSPNCoockiesStandBy.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            //    }
            //else
            //    { 
            //        ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/"))); 
            //    }
            //ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//"click%20on%20%22MODEL%22%232"


            //System.IO.Stream sendStream = tQ.GetRequestStream();
            //sendStream.Write(bytes, 0, bytes.Length);
            //sendStream.Close();
            //HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
            //System.IO.Stream ReceiveStream = tS.GetResponseStream();
            //System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
            //responseFromServer = sr.ReadToEnd();
            //tS.Close();
            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("ADVPartsInfoRequest сработал catch " + responseFromServer);
                responseFromServer = "NO_ANSWER_Your session has expired";
                AddToLogFile("подменили responseFromServer на - " + responseFromServer);
            }
            return responseFromServer;
        }
        // разбивает строку на параметры и записывает их в Dictinary
        private Dictionary<string, string> MakePartDictinary( PartInfoClass rc)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("part_no", "");
            dict.Add("description", "");
            dict.Add("descriptionRU", "");
            dict.Add("specEn", "");
            dict.Add("status", "");
            dict.Add("price", "");
            dict.Add("stockAvail", "");
            dict.Add("division", "");
            dict.Add("divisionDesc", "");
            dict.Add("dealerPrice", "");
            dict.Add("stockAvailQty", "");


            dict["part_no"] = rc.peData.material;
            if (rc.peSuc.materialDesc == "")
            {
                dict["description"] = rc.peData.materialDescEn;
            }
            else
            {
                dict["description"] = rc.peSuc.materialDesc;
            }
            dict["descriptionRU"] = "";
            dict["specEn"] = rc.peData.specEn;
            dict["status"] = rc.peData.salesStatus;
            dict["price"] = rc.peData.salesPrice;
            dict["stockAvail"] = rc.peData.stockAvailDesc;
            dict["division"] = rc.peData.division;
            dict["divisionDesc"] = rc.peData.divisionDesc;
            dict["dealerPrice"] = rc.peData.dealerPrice;
            dict["stockAvailQty"] = rc.peData.stockAvail;

            return dict;
        }

        private Dictionary<string, string> MakePartDictinaryALT(PartInfoClass rc, int seqNo)
        {

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("part_no", "");
            dict.Add("description", "");
            dict.Add("descriptionRU", "");
            dict.Add("specEn", "");
            dict.Add("status", "");
            dict.Add("price", "");
            dict.Add("stockAvail", "");


            dict["part_no"] = rc.ptAltmList[seqNo].material;
            dict["description"] = rc.ptAltmList[seqNo].materialDesc;
            dict["descriptionRU"] = "";
            dict["specEn"] = rc.ptAltmList[seqNo].specEn;
            dict["status"] = rc.ptAltmList[seqNo].salesStatus;
            dict["price"] = rc.ptAltmList[seqNo].salesPrice;
            dict["stockAvail"] = rc.ptAltmList[seqNo].stockAvailDesc;

            return dict;

        }
        //AllWtyInfoBySerialImei
        //стандартный запрос получающий название модели по серийному/имей, возвращает результат запроса
        private string GetModelBySerialRequest(string serialNo)
        {
            string par = "SERIAL_NO=" + serialNo;
      
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/svctracking/svcorder/modelBySerialByGMES2ForNonHHP.gspn";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "application/json, text/javascript, */*; q=0.01";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/svcorder/ServiceOrderCreate.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded";
            tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            //tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ServicePoint.Expect100Continue = false;
            tQ.ContentLength = bytes.Length;
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//"click%20on%20%22MODEL%22%232"

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("GetModelBySerialRequest сработал catch " + responseFromServer);
            }
            return responseFromServer;
            
            
            
        }

        private string GetModelBySerialANDDummyModelRequest(string serialNo, string MOTP)
        {
            //cmd=GetIMEIofHHPCmd&imei=358436075500177&model=SM-G930FZSUSER&system=MPTS&imeiP=&multinum=Y&motp=&src=S
            //string par = "cmd=GetIMEIofHHPCmd&imei=" + serialNo + "&model=GT-I9300&system=MPTS&imeiP=&multinum=Y";
            string par = "cmd=GetIMEIofHHPCmd&imei=" + serialNo + "&model=GT-I9300&system=MPTS&imeiP=&multinum=Y&motp=" + MOTP + "&src=S";
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/svcorder/ServiceOrderCreateEHN.jsp?/svctracking/svcorder/ServiceOrderCreateEHNHHP.jsp?/svctracking/svcorder/ServiceOrderCreate.jsp?menuBlock=&menuUrl=&naviDirValue=&searchContent=&search_status=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded";
            tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            //tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ServicePoint.Expect100Continue = false;
            tQ.ContentLength = bytes.Length;
            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//"click%20on%20%22MODEL%22%232"

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("GetModelBySerialANDDummyModelRequest сработал catch " + responseFromServer);
            }
            return responseFromServer;
        }
        //стандартный запрос получающий описание модели названию, возвращает результат запроса
        private string GetModelDescription(string modelNumber)
        {
            string par = "cmd=ServiceOrderModelSearchCmd&MODEL=" + modelNumber + "&ASC_CODE=0009341483";
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "application/json, text/javascript, */*; q=0.01";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/svcorder/ServiceOrderCreate.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("X-Requested-With", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            //tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ServicePoint.Expect100Continue = false;
            tQ.ContentLength = bytes.Length;
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, ""); //trueclick%20on%20%22MODEL%22%232

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("GetModelDescription сработал catch " + responseFromServer);
            }
            return responseFromServer;
            

        }
        //запрос получающий серийный номер по имею и модели
        private string GetSNByIMEI(string modelNumber, string IMEI, string MOTP)
        {
            //string par = "cmd=GetIMEIofHHPCmd&imei=" + IMEI + "&model=" + modelNumber + "&system=MPTS&imeiP=&multinum=Y";
            string par = "cmd=GetIMEIofHHPCmd&imei=" + IMEI + "&model=" + modelNumber + "&system=MPTS&imeiP=&multinum=Y&motp=" + MOTP + "&src=S";
            string responseFromServer = "";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.Headers.Add("X-Requested-With", "XMLHttpRequest");
            tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/svcorder/ServiceOrderCreate.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.ContentLength = bytes.Length;
            tQ.ServicePoint.Expect100Continue = false;
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//click%20on%20%22MODEL%22%232
            
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
 
            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("GetSNByIMEI сработал catch " + responseFromServer);
            }
            return responseFromServer;
        }
        //получает срок гарантии по модели и серийному
        private string GetWarrantyPeriod(string modelNumber, string serialNo, string purchDate, string ProductType, string IMEI)
        {
            modelNumber = modelNumber.Replace("/", "%2F");
            string responseFromServer="";
            if (serialNo == "") { return responseFromServer; }
            string par = "cmd=ServiceOrderWtyCheckCmd&MODEL=" + modelNumber + "&SERIAL_NO=" + serialNo + "&PURCHASE_DATE=" + purchDate + "&SVC_PRCD=" + ProductType + "&IMEI=" + IMEI + "&CorpCode=C941&SERVICE_TYPE=";

            string url = "https://biz5.samsungcsportal.com/gspn/operate.do?IV_WTY_EXCEPTION=&IV_ASC_CODE=0009341483&USE_PARALLEL=0";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/svcorder/ServiceOrderCreate.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;
            
            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";
            
            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch 
            {
                AddToLogFile("GetWarrantyPeriod сработал catch " + responseFromServer);
            }
            
            return responseFromServer;
        }
        //возвращает последнюю дату из ключевых деталей по сериному номеру
        private string GetKeyInfo(string serialNo)
        {
            //метод устарел и не работает больше на гспн
            //string responseFromServer = "";
            //if (serialNo == "") { return responseFromServer; }
            //if (ZPO == null) 
            //{
            //    GetZPO();
            //    while (ZPO == "")
            //    {
            //        CheckGSPNAuthorization();
            //        GetZPO();
            //    }
            //}
            //else
            //{
            //    TimeSpan DeadLineZPO = new TimeSpan(0, 59, 0);
            //    TimeSpan diff = DateTime.Now - ZPOdate;
            //    if(diff>DeadLineZPO)
            //    { 
            //        GetZPO();
            //        diff = DateTime.Now - ZPOdate;
            //        while (diff > DeadLineZPO)
            //        {
            //            CheckGSPNAuthorization();
            //            GetZPO();
            //        }
            //    }
            //}              

            //string par = "cmd=KeyPartInfoCmd&numPerPage=100&currPage=0&zpo=" + ZPO + "&IV_SERIAL_NUMBER=" + serialNo + "&serial=" + serialNo + "&model=";

            //string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            //HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            //tQ.Method = "POST";
            //byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            //tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            //tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            //tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            //tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            //tQ.Headers.Add("X-Requested-With", "XMLHttpRequest");
            //tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            //tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            //tQ.Referer = "https://biz5.samsungcsportal.com/master/part/KeyPartInfo.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            //tQ.ContentLength = bytes.Length;
            //tQ.ServicePoint.Expect100Continue = false;
            //CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            //ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            //tQ.CookieContainer = new CookieContainer();
            //tQ.CookieContainer = MakeCookie(ckCol, false, "");
            //tQ.Headers.Add("DNT", "1");
            //tQ.KeepAlive = true;

            //try
            //{
            //    System.IO.Stream sendStream = tQ.GetRequestStream();
            //    sendStream.Write(bytes, 0, bytes.Length);
            //    sendStream.Close();
            //    HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
            //    System.IO.Stream ReceiveStream = tS.GetResponseStream();
            //    System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
            //    responseFromServer = sr.ReadToEnd();
            //    tS.Close();
            //}

            //catch
            //{
            //    AddToLogFile("GetKeyInfo сработал catch " + responseFromServer);
            //}

            ////HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
            ////System.IO.Stream ReceiveStream = tS.GetResponseStream();
            ////System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
            ////responseFromServer = sr.ReadToEnd();
            ////tS.Close();
            //return responseFromServer;
            return "fail";
        }
        //возвращает дату из ключевых деталей
        private string NEWGetKeyInfo(string model, string serialNo, string IMEI)
        {
            
            string responseFromServer = "";
            string par = "";
            if (serialNo != "" & IMEI != "")
            {
                par = "cmd=SerialNoCheckPopCmd&numPerPage=100&currPage=0&i_pur_dt=&asc_code=0009341483&country=RU&Model=&Serno=" + serialNo + "&Imei=" + IMEI;
            }
            else
            {
                if (model != "" & serialNo != "")
                {
                    par = "cmd=SerialNoCheckPopCmd&numPerPage=100&currPage=0&i_pur_dt=&asc_code=0009341483&country=RU&Model=" + System.Net.WebUtility.UrlEncode(model).Replace("(", "%28").Replace(")", "%29") + "&Serno=" + serialNo + "&Imei=";
                }
                else
                {
                    return "ThereIsNoEnoughParameters";
                }
            }


            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.Headers.Add("X-Requested-With", "XMLHttpRequest");
            tQ.Headers.Add("X-Prototype-Version", "1.7.2");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Referer = "https://biz5.samsungcsportal.com/wty/wtyclaim/SearchSerial.jsp?i_model=&i_serno=&fldSerno=SERIAL_NO&fldImei=IMEI&fldPurdt=PURCHASE_DATE&form=detailForm&asc_code=0009341483&func=imeiChange&fldModel=MODEL";
            tQ.ContentLength = bytes.Length;
            tQ.ServicePoint.Expect100Continue = false;
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");
            tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("newGetKeyInfo сработал catch " + responseFromServer);
            }

            //HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
            //System.IO.Stream ReceiveStream = tS.GetResponseStream();
            //System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
            //responseFromServer = sr.ReadToEnd();
            //tS.Close();
            return responseFromServer;
        }

        //возвращает информацию о сервис паках
        private string GetADHInfo(string serialNo, string IMEI)
        {
      
            string responseFromServer="";
            if (serialNo == "") { return responseFromServer; }
            string par = "cmd=ZifGspnAdhInfoCmd&IV_GUBUN=&IV_COMPANY=C941&IV_SERIAL_NO=" + serialNo + "&IV_IMEI=" + IMEI;

            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/svctracking/svcorder/ServiceOrderCreate.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;
            
            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";
            
            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch 
            {
                AddToLogFile("GetADHInfo сработал catch " + responseFromServer);
            }
            
            return responseFromServer;
        }
        //возвращает информацию о заказе запчастей
        private string ADVOrderInfoRequest(string confirmNo, string SoldTo, string ShipTo, string fromDate, string toDate)
        {

            string responseFromServer = "";
            if (confirmNo == "") { return responseFromServer; }
            string par = "cmd=PoStatusDetailCmd&numPerPage=100&currPage=0&companyCode=C941&language=R&partFrom=&partTo=&confirmNo=" + confirmNo + "&poIdTo=&poNo=&soShipTo=&customerCode=" + SoldTo + "&shipTo=" + ShipTo + "&fromDate=" + fromDate + "&toDate=" + toDate + "&part=&poIdFrom=";

            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/order/po/POStatus.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("ADVOrderInfoRequest сработал catch " + responseFromServer);
            }

            return responseFromServer;

        }
        //возвращает информацию об отгрузках
        private string POShippingInformationRequest(string ShipTo, string fromDate, string toDate)
        {
            string responseFromServer = "";
            
            string par = "cmd=ShippingInfoCmd&numPerPage=100&currPage=0&indicator=&salesOrg=9414&soNo=&soItemNo=&actType=L&shipTo=" + ShipTo + "&fromDate=" + fromDate + "&toDate=" + toDate + "&confirmNo=&docType=J&ordPart=&grYN=&shipPart=";
            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/order/po/ShipInfo.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("POShippingInformationRequest сработал catch " + responseFromServer);
            }

            return responseFromServer;
        }
        //возвращает информацию о гэлакси диагностике
        private string ADVMGDinfoRequest(string ShipTo, string fromDate, string toDate, string fromDateDots, string toDateDots, string Serial)
        {
            string responseFromServer = "";  
            string par = "cmd=GDDiaResultHeaderCmd&IV_COMPANY=C941&IV_ASC_CODE=" + ShipTo + "&IV_FROM_DATE=" + fromDate + "&IV_TO_DATE=" + toDate + "&numPerPage=100&currPage=0&ASC_CODE=" + ShipTo + "&fromDate=" + fromDateDots + "&toDate=" + toDateDots + "&IV_METHOD=O&IV_RC_FLAG=&IV_SERIAL_NO=" + Serial + "&IV_IMEI=&IV_SO_NO=";

            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/gd/DiagnosisHistory.jsp?search_status=&searchContent=&menuBlock=&menuUrl=&naviDirValue=";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("MGDStatusRequest сработал catch " + responseFromServer);
            }

            return responseFromServer;
        }

        private string ADVMGDDetailRequest(string ShipTo, string IV_OBJECT_ID, string IV_TEMP_OBJECT_ID, string SEQ)
        {
            string responseFromServer = "";
            //cmd=GDDiaResultDetailCmd&IV_COMPANY=C941&IV_OBJECT_ID=4171880249&IV_TEMP_OBJECT_ID=7100235150&IV_SEQ=0000000001&SELECT_ASC=0009341483

            string par = "cmd=GDDiaResultDetailCmd&IV_COMPANY=C941&IV_OBJECT_ID=" + IV_OBJECT_ID + "&IV_TEMP_OBJECT_ID=" + IV_TEMP_OBJECT_ID + "&IV_SEQ=" + SEQ + "&SELECT_ASC=" + ShipTo;

            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/gd/DiagnosisHistoryPop.jsp";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("ADVMGDDetailRequest сработал catch " + responseFromServer);
            }

            return responseFromServer;
        }
        //для Service Part Return
        private string SPR_PrReturnListItemCmd(string SoldTo, string date, string doNo)
        {
            string responseFromServer = "";
            //cmd=PrReturnListItemCmd&currBlock=&currPage=&i_f_date=01.02.2018&i_t_date=04.04.2018&PI_BUKRS=C941&PI_KUNAG=0009341483&PI_ERDAT=04.02.2018&PI_VBELN=8412942479&IV_ASC_ACCTNO=0009341483&IV_ASC_CODE=&IV_VBELN=&IV_TYPE=&IV_SDATE=&IV_EDATE=&i_aubel=&i_wadat_ist=&i_daycnts=&i_bstnk=&i_bstdk=&i_vgbel=&i_remarks1=&sel_partner=0009341483&i_delivery_no=8412942479&i_delivery_date=04.02.2018&i_gi_date=04.02.2018&i_billing_date=04.02.2018&i_order_type=&i_sales_org=&i_distr_channel=&i_division=&i_order_reason=&i_soldto_party=&i_billto_party=&i_payer=&i_shipto_party=&i_postal_code=&i_city=&i_street=&i_country_code=&i_item_summary=&i_h_currency=&i_reference_billing_no=&i_inco_terms_1=&i_inco_terms_2=&i_payment_terms_2=&i_payment_terms_1=

            string par = "cmd=PrReturnListItemCmd&currBlock=&currPage=&i_f_date=" + date + "&i_t_date=" + date + "&PI_BUKRS=C941&PI_KUNAG=" + SoldTo + "&PI_ERDAT=" + date + "&PI_VBELN=" + doNo + "&IV_ASC_ACCTNO=" + SoldTo + "&IV_ASC_CODE=&IV_VBELN=&IV_TYPE=&IV_SDATE=&IV_EDATE=&i_aubel=&i_wadat_ist=&i_daycnts=&i_bstnk=&i_bstdk=&i_vgbel=&i_remarks1=&sel_partner=" + SoldTo + "&i_delivery_no=" + doNo + "&i_delivery_date=" + date + "&i_gi_date=" + date + "&i_billing_date=" + date + "&i_order_type=&i_sales_org=&i_distr_channel=&i_division=&i_order_reason=&i_soldto_party=&i_billto_party=&i_payer=&i_shipto_party=&i_postal_code=&i_city=&i_street=&i_country_code=&i_item_summary=&i_h_currency=&i_reference_billing_no=&i_inco_terms_1=&i_inco_terms_2=&i_payment_terms_2=&i_payment_terms_1=";

            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/pr/PrReturnListItem.jsp";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("SPR_PrReturnListItemCmd сработал catch " + responseFromServer);
            }

            return responseFromServer;
        }

        private string SPR_CreditRequestVerifyCmd(string parametr)
        {
            string responseFromServer = "";

            string par = "cmd=CreditRequestVerifyCmd&action_type=PR_CREATE_VERIFY&" + parametr;


            string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            tQ.Headers.Add("Accept-Language", "ru");
            tQ.Headers.Add("x-prototype-version", "1.7.2");
            tQ.Referer = "https://biz5.samsungcsportal.com/pr/CreditRequest.jsp";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("SPR_CreditRequestVerifyCmd сработал catch " + responseFromServer);
            }

            return responseFromServer;
        }

        private string SRP_CreditRequestSaveCmd(string parametr)
            {
                string responseFromServer = "";

                string par = "cmd=CreditRequestSaveCmd&action_type=PR_CREATE&" + parametr;


                string url = "https://biz5.samsungcsportal.com/gspn/operate.do";
                HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
                tQ.Method = "POST";
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
                tQ.Accept = "application/x-ms-application, image/jpeg, application/xaml+xml, image/gif, image/pjpeg, application/x-ms-xbap, */*";
                tQ.Referer = "https://biz5.samsungcsportal.com/pr/CreditRequest.jsp";
                tQ.Headers.Add("Accept-Language", "ru-RU");
                tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            //tQ.Headers.Add("x-prototype-version", "1.7.2");
           // Content - Type: application / x - www - form - urlencoded
                //tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
                tQ.ContentType = "application/x-www-form-urlencoded";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate");
                
                tQ.ContentLength = bytes.Length;

                tQ.ServicePoint.Expect100Continue = false;
                tQ.Headers.Add("Cache-Control", "no-cache");
                CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
                ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
                tQ.CookieContainer = new CookieContainer();
                tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

                //tQ.Headers.Add("DNT", "1");
                tQ.KeepAlive = true;
                //tQ.Connection = "close";

                try
                {
                    System.IO.Stream sendStream = tQ.GetRequestStream();
                    sendStream.Write(bytes, 0, bytes.Length);
                    sendStream.Close();
                    HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                    System.IO.Stream ReceiveStream = tS.GetResponseStream();
                    System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                    responseFromServer = sr.ReadToEnd();
                    tS.Close();
                }

                catch
                {
                    AddToLogFile("SRP_CreditRequestSaveCmd сработал catch " + responseFromServer);
                }

                return responseFromServer;
            }

        private string ADV_IMEIFabricInfoRequest(string Serial)
        {
            string responseFromServer = "";
            string par = "TYPE=I&SERIAL_NO=" + Serial + "&HHPYN=Y";

            string url = "https://biz5.samsungcsportal.com/svctracking/svcorder/modelBySerialByGMES2ForNonHHP.gspn";
            HttpWebRequest tQ = (HttpWebRequest)HttpWebRequest.Create(url);
            tQ.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(par);
            tQ.Accept = "application/json, text/javascript, */*; q=0.01";
            tQ.Referer = "https://biz5.samsungcsportal.com/gspn/operate.do?cmd=ZifGspnSvcMainLDCmd&objectID=4172578543";
            tQ.Headers.Add("x-requested-with", "XMLHttpRequest");
            tQ.ContentType = "application/x-www-form-urlencoded";
            tQ.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            tQ.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";
            tQ.ContentLength = bytes.Length;

            tQ.ServicePoint.Expect100Continue = false;
            tQ.Headers.Add("Cache-Control", "no-cache");
            CookieCollection ckCol = new CookieCollection(); //создаем коллекцию для хранения
            ckCol.Add(GSPNCoockies.GetCookies(new Uri("https://biz5.samsungcsportal.com/")));
            tQ.CookieContainer = new CookieContainer();
            tQ.CookieContainer = MakeCookie(ckCol, false, "");//#click%20on%20%22%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%B3%D0%B0%D1%80%D0%B0%D0%BD%D1%82%D0%B8%D0%B8%22

            //tQ.Headers.Add("DNT", "1");
            tQ.KeepAlive = true;
            //tQ.Connection = "close";

            try
            {
                System.IO.Stream sendStream = tQ.GetRequestStream();
                sendStream.Write(bytes, 0, bytes.Length);
                sendStream.Close();
                HttpWebResponse tS = (HttpWebResponse)tQ.GetResponse();
                System.IO.Stream ReceiveStream = tS.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);
                responseFromServer = sr.ReadToEnd();
                tS.Close();
            }

            catch
            {
                AddToLogFile("ADV_IMEIFabricInfoRequest сработал catch " + responseFromServer);
            }

            return responseFromServer;
        }


    }

    public class Server
    {
        TcpListener Listener; // Объект, принимающий TCP-клиентов
        public bool ServerWorks = false;
        public bool IsRunning { get; set; }
        public int Port { get; set; }

        private void AddToLogFile(string LOGvalue)
        {
            string writePath = "www/log.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(DateTime.Now + " " + LOGvalue);
                    //sw.Write(4.5);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        
        // Запуск сервера
        public Server(int Port)
        {
            this.Port = Port;
            this.IsRunning = true;
            //this.StartServer();
        }

        public void StartServer()
        {
            
            Listener = new TcpListener(IPAddress.Any, Port); // Создаем "слушателя" для указанного порта
            Listener.Start(); // Запускаем его
            AddToLogFile("Запущен сервер на порту " + Port.ToString());
            // В бесконечном цикле
            while (true)
            {
                // Принимаем новых клиентов. После того, как клиент был принят, он передается в новый поток (ClientThread)
                // с использованием пула потоков.
                //ThreadPool.QueueUserWorkItem(new WaitCallback(ClientThread), Listener.AcceptTcpClient());



                // Принимаем нового клиента
                TcpClient Client = Listener.AcceptTcpClient();
                if (IsRunning == false) 
                { Listener.Stop();
                    
                    string Html = "<html><body><h1>200</h1></body></html>";
                    string Headers = "HTTP/1.1 200 OK\nContent-Type: text/html" + "\nContent-Length: " + Html.Length + "\n\n";
                    byte[] HeadersBuffer = Encoding.ASCII.GetBytes(Headers);
                    Client.GetStream().Write(HeadersBuffer, 0, HeadersBuffer.Length); 
                    //int Code = 200;
                   // string CodeStr = Code.ToString() + " " + ((HttpStatusCode)Code).ToString();
                    // Код простой HTML-странички
                    //string Html = "<html><body><h1>" + CodeStr + "</h1></body></html>";
                    // Необходимые заголовки: ответ сервера, тип и длина содержимого. После двух пустых строк - само содержимое
                    //string Str = "HTTP/1.1 " + CodeStr + "\nContent-type: text/html\nContent-Length:" + Html.Length.ToString() + "\n\n" + Html;
                    // Приведем строку к виду массива байт
                    //byte[] Buffer = Encoding.ASCII.GetBytes(Str);
                    // Отправим его клиенту
                    //Client.GetStream().Write(Buffer, 0, Buffer.Length);
                    // Закроем соединение
                    Client.Close();
                    break; 
                }
                // И запускаем этот поток, передавая ему принятого клиента
                // Создаем поток
                Thread Thread = new Thread(new ParameterizedThreadStart(ClientThread));
                // И запускаем этот поток, передавая ему принятого клиента
                Thread.Start(Client);
                Thread.Join();
            }
        }

        static void ClientThread(Object StateInfo)
        {
            // Просто создаем новый экземпляр класса Client и передаем ему приведенный к классу TcpClient объект StateInfo
            new Client((TcpClient)StateInfo);
        }

        // Остановка сервера
        ~Server()
        {
            // Если "слушатель" был создан
            if (Listener != null)
            {
                // Остановим его
                Listener.Stop();
            }
        }

        //static void Main(string[] args)
        //{
        //    // Определим нужное максимальное количество потоков
        //    // Пусть будет по 4 на каждый процессор
        //    //int MaxThreadsCount = Environment.ProcessorCount * 4;
        //    // Установим максимальное количество рабочих потоков
        //    //ThreadPool.SetMaxThreads(MaxThreadsCount, MaxThreadsCount);
        //    // Установим минимальное количество рабочих потоков
        //    //ThreadPool.SetMinThreads(2, 2);
        //    // Создадим новый сервер на порту 21770
        //    new Server(21770);
        //}
    }

    //классы для десиарелизации

    //\/shipping info
    class RootClassShippingInfo
    {
        public string returnCode { get; set; }
        public string totDoQty { get; set; }
        public bool error { get; set; }
        public string currentStatus { get; set; }
        public string returnMessage { get; set; }
        public bool success { get; set; }
        public List<PtDataList> PtDataList { get; set; }
    }

    class PtDataList
    {
        public string invoiceDate { get; set; }
        public string grStatus { get; set; }
        public List<trkInfo> trkInfo { get; set; }
        public string deliveryNo { get; set; }
        public string message1 { get; set; }
        public string invoiceNo { get; set; }
        public string message2 { get; set; }
        public string storageLocation { get; set; }
        public string amount { get; set; }
        public string poDate { get; set; }
        public string invoiceItemNo { get; set; }
        public string shipCondition { get; set; }
        public string shipPartDiv { get; set; }
        public string partLocalDesc { get; set; }
        public string invoiceDateOrig { get; set; }
        public string type1 { get; set; }
        public string soItemNo { get; set; }
        public string ordPartDiv { get; set; }
        public string shipTo { get; set; }
        public string localInvNo { get; set; }
        public string type2 { get; set; }
        public string wadatSit { get; set; }
        public string deliveryDate { get; set; }
        public string shipPointDesc { get; set; }
        public string shippedPart { get; set; }
        public string soNo { get; set; }
        public string status { get; set; }
        public string shipConDesc { get; set; }
        public string poNo { get; set; }
        public string tax { get; set; }
        public string corePrice { get; set; }
        public string orderQty { get; set; }
        public string plant { get; set; }
        public string giDate { get; set; }
        public string accountingDoc { get; set; }
        public string currency  { get; set; }
        public string shipType { get; set; }
        public string batchNo { get; set; }
        public List<trkEvet> trkEvet { get; set; }
        public string relevance { get; set; }
        public string custPoNo { get; set; }
        public string doQty { get; set; }
        public string orderedPart { get; set; }
        public string deliveryItemNo { get; set; }
        public string coreFlag { get; set; }
        public string shipPartDesc { get; set; }
        public string qtyUnit { get; set; }
        public string shipPoint { get; set; }
        public string cntOfBatchNo { get; set; }
        public string netValue { get; set; }
        public string poItemNo { get; set; }
        public string ordPartDesc { get; set; }
        public string shipTypeDesc { get; set; }

        public override string ToString()
        {
            return String.Format("{0} -- deliveryNo, {1} -- invoiceDate", deliveryNo, invoiceDate );
        }
    }

    class trkInfo
    {
        //public string something1 { get; set; }
        //public string something2 { get; set; }
    }

    class trkEvet
    {
        public string doNo { get; set; }
        public string trstatus_desc { get; set; }
        public string trackingNo { get; set; }
        public string trstdate { get; set; }
        public string trstatus { get; set; }
        public string trstperson { get; set; }
        public string trsttime { get; set; }
    }
    ///\


    //\/GET_ADH
    class RootClassGetADH
    {
        public string EV_RET_MSG { get; set; }
        public esSmcInfo esSmcInfo { get; set; }
        public string error { get; set; }
        public string EV_RET_CODE { get; set; }
        public eS_RESULT eS_RESULT { get; set; }
        public bool success { get; set; }
  
    }

    class esSmcInfo
    {
        public string policyId { get; set; }
        public string lossTypeId { get; set; }
        public string damageExplanation { get; set; }
    }

    class eS_RESULT
    {
        public string SL_CD { get; set; }
        public string KUNNR { get; set; }
        public string WAERS { get; set; }
        public string BLOC { get; set; }
        public string e_DAT { get; set; }
        public string s_DAT { get; set; }
        public string SL_TX { get; set; }
        public string STATUS { get; set; }
        public string CERTI { get; set; }
        public string PURCHASE_DATE { get; set; }
        public string ST_CD { get; set; }
        public string ADH_CHARGE_CUST { get; set; }
        public string ADH_IND { get; set; }
        public string SERNO { get; set; }
        public string DESCR { get; set; }
        public string RESULT_ADH { get; set; }
        public string COMPANY { get; set; }
        public string CONTRACT_DATE { get; set; }
        public string BALANCE { get; set; }
        public string PT_TX { get; set; }
        public string ADH_COUNT { get; set; }
        public string PACK_TYPE { get; set; }
        public string PT_CD { get; set; }
        public string KNAME { get; set; }
        public string CONTRACT { get; set; }
        public string REGDT { get; set; }
        public string MODEL { get; set; }
        public string END_USER { get; set; }
        public string CHARGE_AMOUNT { get; set; }
        public string ST_TX { get; set; }
        public string PACNO { get; set; }
    }
    ///\

    //\/ADV_ORDER_INFORMATION

    class RootClassOrderInformation
    {
        public bool error { get; set; }
        public bool updatable { get; set; }
        public List<soList> soList { get; set; }
        public bool success { get; set; }

    }

    class soList
    {
        public string model { get; set; }
        public string shipTrDesc { get; set; }
        public string wtyBillNo { get; set; }
        public string reason { get; set; }
        public string remark { get; set; }
        public string etd { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string type { get; set; }
        public string currency { get; set; }
        public string poDate { get; set; }
        public string amount { get; set; }
        public string rejectCode { get; set; }
        public string billNo { get; set; }
        public string soItemNo { get; set; }
        public string customerCode { get; set; }
        public string amountTax { get; set; }
        public string shipTo { get; set; }
        public string poType { get; set; }
        public string custPoNo { get; set; }
        public string completed { get; set; }
        public string doQty { get; set; }
        public string soNo { get; set; }
        public string changeYn { get; set; }
        public string status { get; set; }
        public string poQty { get; set; }
        public string soPart { get; set; }
        public string provider { get; set; }
        public string poPart { get; set; }
        public string baseQty { get; set; }
        public string poNo { get; set; }
        public string changeDate { get; set; }
        public string ticketNo { get; set; }
        public string price { get; set; }
        public string compliant { get; set; }
        public string refNo { get; set; }
        public string poItemNo { get; set; }
        public string returnFlag { get; set; }
        public string descr { get; set; }
        public string serial { get; set; }
        public string success { get; set; }
        public string billQty { get; set; }
    }
    //\ADV_ORDER_INFORMATION

    //\/ADV_MGDInfo
    class RootClassMGDStatus
    {
        public bool error { get; set; }
        public string EV_RET_CODE { get; set; }
        public List<ET_DIA_HISTORY> ET_DIA_HISTORY { get; set; }
        public eS_RESULT eS_RESULT { get; set; }
        public bool success { get; set; }

    }

    class ET_DIA_HISTORY
    {
        public string TEMP_OBJECT_ID { get; set; }
        public string MODEL { get; set; }
        public string IMEI { get; set; }
        public string COMPLETE_DT { get; set; }
        public string CHECK_RESULT { get; set; }
        public string ERZET { get; set; }
        public string SERIAL_NO { get; set; }
        public string SEQ { get; set; }
        public string OBJECT_ID { get; set; }
        public string METHOD { get; set; }
        public string METHOD_SEQ { get; set; }
        public string ERDAT { get; set; }
    }
    ///\ADV_MGDInfo

    //\/ADV_MGD_Detail
    class RootClassMGDDetail
    {
        public string EV_RET_MSG { get; set; }
        public List<custList> custList { get; set; }
        public bool error { get; set; }
        public string EV_CHECK_RESULT_H { get; set; }
        public string EV_RET_CODE { get; set; }
        public bool success { get; set; }
        public List<ET_DIA_DETAIL> ET_DIA_DETAIL { get; set; }
        
    }
    class custList
    {
        //не заполняю так как там шлак всякий не нужный
    }
    class ET_DIA_DETAIL
    {
        public string REFERENCE_VALUE { get; set; }
        public string GD_GROUP { get; set; }
        public string GD_GROUP_DESC { get; set; }
        public string CHECK_RESULT { get; set; }
        public string GD_SUB_CODE { get; set; }
        public string CHECK_TIME { get; set; }
        public string GD_SUB_DESC { get; set; }
        public string JOB_FLAG { get; set; }
    }

  
    //ADV_SPR Service Parts Return
    class RootPrReturnListItemCmd
    {
        public bool error { get; set; }
        public List<ptHeader> ptHeader { get; set; }
        public List<ptItem> ptItem { get; set; }
        public List<ptOutput> ptOutput { get; set; }
        public string retcode { get; set; }
        public string retmsg { get; set; }
        public bool success { get; set; }

    }

    class ptHeader
    {
        public string vkorg { get; set; }
        public string vtweg { get; set; }
        public string spart { get; set; }
        public string pstlz { get; set; }
        public string ort01 { get; set; }
        public string stras { get; set; }
        public string land1 { get; set; }
        public string netwr { get; set; }
        public string waers { get; set; }
        public string inco1 { get; set; }
        public string inco2 { get; set; }
        public string text1 { get; set; }
        public string zterm { get; set; }


    }

    class ptItem
    {
        public string arktx { get; set; }
        public string lgort { get; set; }
        public string netpr { get; set; }
        public string netwr { get; set; }
        public string vrkme { get; set; }
        public string waers { get; set; }
        public string werks { get; set; }
        public string prsdt { get; set; }
        public string posnr { get; set; }
        public string matnr { get; set; }
    }

    class ptOutput
    {
        //не заполняю т.к. не надо
    }
    //SPR_CreditRequestVerifyCmd

    class RootSPR_CreditRequestVerifyCmd

    {
        public bool error { get; set; }
        public string retcode { get; set; }
        public string retmsg { get; set; }
        public bool success { get; set; }
    }


    //NEWGetKeyInfo
    class RootKeyPartsInfo
    {
        public bool error { get; set; }
        public List<ptModel> ptModel { get; set; }
        public List<ptWdata> ptWdata { get; set; }
        public string returnCode { get; set; }
        public string returnMessage{ get; set; }
        public bool success { get; set; }
    }

    class ptModel
    {

        //не заполняю т.к. нет нужной информации
    }

    class ptWdata
    {
        public string comp_dt{ get; set; }
        public string sub_term { get; set; }
        public string pur_dt { get; set; }
        
    }

    //PartInfo\\//

    class PartInfoClass
    {
        public List<ptUpperList> ptUpperList { get; set; }
        public peData peData { get; set; }
        public peLocation peLocation { get; set; }
        public bool error { get; set; }
        public List<ptLowerList> ptLowerList { get; set; }
        public List<ptAltmList> ptAltmList { get; set; }
        public peSuc peSuc { get; set; }
        public bool success { get; set; }
        public List<ptCosmosList> ptCosmosList { get; set; }
    }

    class ptUpperList
    {
   
        //пустой
    }
    class peData
    {
        public string spec { get; set; }
        public string materialDesc { get; set; }
        public string localCurrency { get; set; }
        public string registeredDate { get; set; }
        public string rohsStockAvail { get; set; }
        public string lastDate { get; set; }
        public string coreAPrice { get; set; }
        public string unitPrice { get; set; }
        public string dealerPrice { get; set; }
        public string recommendPrice { get; set; }
        public string currency { get; set; }
        public string baseUnit { get; set; }
        public string wtyTerm { get; set; }
        public string retailPrice { get; set; }
        public string cnaRegDate { get; set; }
        public string primePart { get; set; }
        public string division { get; set; }
        public string etdDate { get; set; }
        public string stockAvailDesc { get; set; }
        public string gpcDate { get; set; }
        public string salesStatus { get; set; }
        public string materialDescEn { get; set; }
        public string divisionDesc { get; set; }
        public string coreBPrice { get; set; }
        public string salesPrice { get; set; }
        public string localRecommendPrice { get; set; }
        public string etdMsg { get; set; }
        public string stockAvail { get; set; }
        public string localSalesPrice { get; set; }
        public string material { get; set; }
        public string mstDv { get; set; }
        public string specEn { get; set; }
        public string color { get; set; }
        public string nonRohsStockAvail { get; set; }
        public string vendorETA { get; set; }
    }
    class peLocation
    {
        public string strorageBin { get; set; }
    }
    class ptLowerList
    { 
        //пустой
    }
    class ptAltmList
    {

        public string spec { get; set; }
        public string salesPrice { get; set; }
        public string materialDesc { get; set; }
        public string gubun { get; set; }
        public string engStock { get; set; }
        public string rohsStockAvail { get; set; }
        public string dealerPrice { get; set; }
        public string recommendPrice { get; set; }
        public string availableQty { get; set; }
        public string material { get; set; }
        public string primePartYn { get; set; }
        public string currency { get; set; }
        public string baseUnit { get; set; }
        public string primePart { get; set; }
        public string specEn { get; set; }
        public string color { get; set; }
        public string stockAvailDesc { get; set; }
        public string salesStatus { get; set; }
        public string materialDescEn { get; set; }
        public string nonRohsStockAvail { get; set; }
        public string flatFlag { get; set; }
        public string vendorETA { get; set; }

    }
    class peSuc
    {

        public string createdOn { get; set; }
        public string baseUnit { get; set; }
        public string atPqt { get; set; }
        public string cpMaterialStatus { get; set; }
        public string materialDesc { get; set; }
        public string cdcMaterialStatus { get; set; }
        public string unrestictedUseStock { get; set; }
        public string lastChangeDate { get; set; }
        public string materialGroup { get; set; }
        public string materialStatusDate { get; set; }
        public string material { get; set; }

    }
    class ptCosmosList
    {
        //пустой
    }
    //PartInfo //\\






}
